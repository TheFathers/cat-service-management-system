INSERT INTO smartgarage.users (username, first_name, last_name, phone, password, email)
VALUES ('cdimitrov', 'Chavdar', 'Dimitrov', '0885810921', '123', 'chavdardim@gmail.com'),
       ('istavrev', 'Ivaylo', 'Stavrev', '0855233421', '12345', 'litosarm@gmail.com');

INSERT INTO smartgarage.cars (license_plate, vin, creation_year, brand, carModel, user_id)
VALUES ('CB3449HB', 'JTDAT1230Y5017131', 2005, 'Toyota', 'Yaris', 1),
       ('A8695MT', 'KMHDU4AD4AU955646', 2011, 'Opel', 'Zafira', 2);

INSERT INTO smartgarage.visits (check_in, check_out, car_id, user_id)
VALUES ('2022-03-02 13:17:07.0', '2022-03-04 13:17:18.0', 2, 2),
       ('2022-03-01 13:17:07.0', '2022-03-03 13:17:38.0', 1, 1);


INSERT INTO smartgarage.roles (`role`)
VALUES ('Customer'),
       ('Employee');



INSERT INTO smartgarage.users_roles (user_id, role_id)
VALUES (1, 2),
       (2, 1);


INSERT INTO smartgarage.car_services (name, price)
VALUES ('Oil change', 45.0),
       ('Tyre change', 67.0);

INSERT INTO smartgarage.visits_services (visit_id, service_id)
VALUES (2, 1),
       (2, 2),
       (1, 2);

