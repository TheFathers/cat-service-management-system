create or replace table brands
(
    brand_id  int auto_increment
        primary key,
    name      varchar(32)          not null,
    is_active tinyint(1) default 1 not null,
    constraint brands_name_uindex
        unique (name)
)
    charset = latin1;

create or replace table models
(
    model_id  int auto_increment
        primary key,
    name      varchar(32)          not null,
    is_active tinyint(1) default 1 not null,
    constraint models_name_uindex
        unique (name)
)
    charset = latin1;

create or replace table brands_models
(
    brand_id int null,
    model_id int null,
    constraint brands_models_brands_brand_id_fk
        foreign key (brand_id) references brands (brand_id),
    constraint brands_models_models_model_id_fk
        foreign key (model_id) references models (model_id)
)
    charset = latin1;

create or replace table roles
(
    role_id int auto_increment
        primary key,
    role    varchar(16) not null
)
    charset = latin1;

create or replace index employees_users_user_id_fk
    on roles (role);

create or replace table services
(
    service_id int auto_increment
        primary key,
    name       varchar(64)                 not null,
    price      double                      not null,
    status     enum ('ACTIVE', 'INACTIVE') not null
)
    charset = latin1;

create or replace table users
(
    user_id                 int auto_increment
        primary key,
    username                varchar(32)                              not null,
    first_name              varchar(32)                              null,
    last_name               varchar(32)                              null,
    phone                   varchar(32)                              null,
    password                char(60)                                 not null,
    email                   varchar(32)                              null,
    currency                enum ('BGN', 'EUR', 'USD') default 'BGN' not null,
    enabled                 tinyint(1)                               not null,
    join_date               date                                     not null,
    last_login_date         date                                     null,
    last_login_date_display date                                     null
)
    charset = latin1;

create or replace table cars
(
    car_id        int auto_increment
        primary key,
    license_plate varchar(32) not null,
    vin           varchar(32) not null,
    creation_year year        null,
    user_id       int         null,
    constraint cars_users_user_id_fk
        foreign key (user_id) references users (user_id)
)
    charset = latin1;

create or replace table cars_brands
(
    car_id   int not null,
    brand_id int not null,
    constraint cars_brands_brands_brand_id_fk
        foreign key (brand_id) references brands (brand_id),
    constraint cars_brands_cars_car_id_fk
        foreign key (car_id) references cars (car_id)
)
    charset = latin1;

create or replace table cars_models
(
    car_id   int not null,
    model_id int not null,
    constraint cars_models_cars_car_id_fk
        foreign key (car_id) references cars (car_id),
    constraint cars_models_models_model_id_fk
        foreign key (model_id) references models (model_id)
)
    charset = latin1;

create or replace table password_tokens
(
    password_token_id int auto_increment
        primary key,
    token             varchar(60) not null,
    user_id           int         not null,
    expiry_date       date        not null,
    constraint password_tokens_users_user_id_fk
        foreign key (user_id) references users (user_id)
)
    charset = latin1;

create or replace table tokens
(
    token_id    int auto_increment
        primary key,
    token       varchar(60) not null,
    user_id     int         not null,
    expiry_date date        not null,
    constraint tokens_users_user_id_fk
        foreign key (user_id) references users (user_id)
)
    charset = latin1;

create or replace table users_roles
(
    user_id int null,
    role_id int null,
    constraint users_roles_roles_role_id_fk
        foreign key (role_id) references roles (role_id),
    constraint users_roles_users_user_id_fk
        foreign key (user_id) references users (user_id)
)
    charset = latin1;

create or replace table visits
(
    visit_id  int auto_increment
        primary key,
    check_in  datetime not null,
    check_out datetime null,
    car_id    int      not null,
    user_id   int      null,
    constraint visits_cars_car_id_fk
        foreign key (car_id) references cars (car_id),
    constraint visits_users_user_id_fk
        foreign key (user_id) references users (user_id)
)
    charset = latin1;

create or replace table visits_services
(
    visit_id   int null,
    service_id int null,
    constraint cars_services_services_service_id_fk
        foreign key (service_id) references services (service_id),
    constraint visits_services_visits_visit_id_fk
        foreign key (visit_id) references visits (visit_id)
)
    charset = latin1;

