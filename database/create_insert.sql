-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.6.5-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for smartgarage
CREATE DATABASE IF NOT EXISTS `smartgarage` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `smartgarage`;

-- Dumping structure for table smartgarage.brands
CREATE TABLE IF NOT EXISTS `brands` (
                                        `brand_id` int(11) NOT NULL AUTO_INCREMENT,
                                        `name` varchar(32) NOT NULL,
                                        PRIMARY KEY (`brand_id`),
                                        UNIQUE KEY `brands_name_uindex` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- Dumping data for table smartgarage.brands: ~12 rows (approximately)
/*!40000 ALTER TABLE `brands` DISABLE KEYS */;
INSERT INTO `brands` (`brand_id`, `name`) VALUES
                                              (7, 'Audi'),
                                              (6, 'BMW'),
                                              (15, 'Dodge'),
                                              (5, 'Ford'),
                                              (14, 'Jaguar'),
                                              (8, 'Mercedes Benz'),
                                              (2, 'Opel'),
                                              (11, 'Peugeot'),
                                              (9, 'Pontiac'),
                                              (10, 'Renault'),
                                              (12, 'Tank T-72'),
                                              (1, 'Toyota');
/*!40000 ALTER TABLE `brands` ENABLE KEYS */;

-- Dumping structure for table smartgarage.brands_models
CREATE TABLE IF NOT EXISTS `brands_models` (
                                               `brand_id` int(11) DEFAULT NULL,
                                               `model_id` int(11) DEFAULT NULL,
                                               KEY `brands_models_brands_brand_id_fk` (`brand_id`),
                                               KEY `brands_models_models_model_id_fk` (`model_id`),
                                               CONSTRAINT `brands_models_brands_brand_id_fk` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`brand_id`),
                                               CONSTRAINT `brands_models_models_model_id_fk` FOREIGN KEY (`model_id`) REFERENCES `models` (`model_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table smartgarage.brands_models: ~25 rows (approximately)
/*!40000 ALTER TABLE `brands_models` DISABLE KEYS */;
INSERT INTO `brands_models` (`brand_id`, `model_id`) VALUES
                                                         (1, 1),
                                                         (1, 3),
                                                         (1, 20),
                                                         (1, 21),
                                                         (1, 22),
                                                         (1, 23),
                                                         (2, 2),
                                                         (2, 16),
                                                         (2, 17),
                                                         (2, 18),
                                                         (2, 19),
                                                         (14, 13),
                                                         (15, 14),
                                                         (8, 15),
                                                         (8, 6),
                                                         (8, 7),
                                                         (6, 4),
                                                         (6, 5),
                                                         (10, 9),
                                                         (11, 10),
                                                         (11, 11),
                                                         (5, 24),
                                                         (5, 25),
                                                         (5, 26),
                                                         (5, 27);
/*!40000 ALTER TABLE `brands_models` ENABLE KEYS */;

-- Dumping structure for table smartgarage.cars
CREATE TABLE IF NOT EXISTS `cars` (
                                      `car_id` int(11) NOT NULL AUTO_INCREMENT,
                                      `license_plate` varchar(32) NOT NULL,
                                      `vin` varchar(32) NOT NULL,
                                      `creation_year` year(4) DEFAULT NULL,
                                      `user_id` int(11) DEFAULT NULL,
                                      PRIMARY KEY (`car_id`),
                                      KEY `cars_users_user_id_fk` (`user_id`),
                                      CONSTRAINT `cars_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table smartgarage.cars: ~5 rows (approximately)
/*!40000 ALTER TABLE `cars` DISABLE KEYS */;
INSERT INTO `cars` (`car_id`, `license_plate`, `vin`, `creation_year`, `user_id`) VALUES
                                                                                      (1, 'CB3449HB', 'JTDAT1230Y5017131', '2005', 1),
                                                                                      (2, 'A8695MT', 'KMHDU4AD4AU955646', '2011', 2),
                                                                                      (4, 'CA1212XA', 'SAJWA1C78D8V38055', '2013', 3),
                                                                                      (5, 'CA4545MB', '1B7GG23Y1NS526835', '1992', 4),
                                                                                      (6, 'CB0833HK', 'WDBRF52H76F783280', '2006', 5);
/*!40000 ALTER TABLE `cars` ENABLE KEYS */;

-- Dumping structure for table smartgarage.cars_brands
CREATE TABLE IF NOT EXISTS `cars_brands` (
                                             `car_id` int(11) NOT NULL,
                                             `brand_id` int(11) NOT NULL,
                                             KEY `cars_brands_cars_car_id_fk` (`car_id`),
                                             KEY `cars_brands_brands_brand_id_fk` (`brand_id`),
                                             CONSTRAINT `cars_brands_brands_brand_id_fk` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`brand_id`),
                                             CONSTRAINT `cars_brands_cars_car_id_fk` FOREIGN KEY (`car_id`) REFERENCES `cars` (`car_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table smartgarage.cars_brands: ~5 rows (approximately)
/*!40000 ALTER TABLE `cars_brands` DISABLE KEYS */;
INSERT INTO `cars_brands` (`car_id`, `brand_id`) VALUES
                                                     (1, 1),
                                                     (2, 2),
                                                     (4, 14),
                                                     (5, 15),
                                                     (6, 8);
/*!40000 ALTER TABLE `cars_brands` ENABLE KEYS */;

-- Dumping structure for table smartgarage.cars_models
CREATE TABLE IF NOT EXISTS `cars_models` (
                                             `car_id` int(11) NOT NULL,
                                             `model_id` int(11) NOT NULL,
                                             KEY `cars_models_models_model_id_fk` (`model_id`),
                                             KEY `cars_models_cars_car_id_fk` (`car_id`),
                                             CONSTRAINT `cars_models_cars_car_id_fk` FOREIGN KEY (`car_id`) REFERENCES `cars` (`car_id`),
                                             CONSTRAINT `cars_models_models_model_id_fk` FOREIGN KEY (`model_id`) REFERENCES `models` (`model_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table smartgarage.cars_models: ~5 rows (approximately)
/*!40000 ALTER TABLE `cars_models` DISABLE KEYS */;
INSERT INTO `cars_models` (`car_id`, `model_id`) VALUES
                                                     (1, 1),
                                                     (2, 2),
                                                     (4, 13),
                                                     (5, 14),
                                                     (6, 15);
/*!40000 ALTER TABLE `cars_models` ENABLE KEYS */;

-- Dumping structure for table smartgarage.models
CREATE TABLE IF NOT EXISTS `models` (
                                        `model_id` int(11) NOT NULL AUTO_INCREMENT,
                                        `name` varchar(32) NOT NULL,
                                        PRIMARY KEY (`model_id`),
                                        UNIQUE KEY `models_name_uindex` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

-- Dumping data for table smartgarage.models: ~26 rows (approximately)
/*!40000 ALTER TABLE `models` DISABLE KEYS */;
INSERT INTO `models` (`model_id`, `name`) VALUES
                                              (10, '206'),
                                              (11, '307'),
                                              (22, 'Avensis'),
                                              (20, 'Aygo'),
                                              (15, 'C Class'),
                                              (7, 'CLK 270'),
                                              (3, 'Corola'),
                                              (8, 'Cougar'),
                                              (14, 'Dakota'),
                                              (26, 'Fiesta'),
                                              (24, 'Focus'),
                                              (6, 'GL500'),
                                              (21, 'Hilux'),
                                              (19, 'Insignia'),
                                              (16, 'Kadett'),
                                              (9, 'Megane'),
                                              (25, 'Mondeo'),
                                              (27, 'Mustang'),
                                              (23, 'Prius'),
                                              (17, 'Tigra'),
                                              (18, 'Vectra'),
                                              (4, 'X3'),
                                              (5, 'X5'),
                                              (13, 'XJ'),
                                              (1, 'Yaris'),
                                              (2, 'Zafira');
/*!40000 ALTER TABLE `models` ENABLE KEYS */;

-- Dumping structure for table smartgarage.password_tokens
CREATE TABLE IF NOT EXISTS `password_tokens` (
                                                 `password_token_id` int(11) NOT NULL AUTO_INCREMENT,
                                                 `token` varchar(60) NOT NULL,
                                                 `user_id` int(11) NOT NULL,
                                                 `expiry_date` date NOT NULL,
                                                 PRIMARY KEY (`password_token_id`),
                                                 KEY `password_tokens_users_user_id_fk` (`user_id`),
                                                 CONSTRAINT `password_tokens_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table smartgarage.password_tokens: ~0 rows (approximately)
/*!40000 ALTER TABLE `password_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_tokens` ENABLE KEYS */;

-- Dumping structure for table smartgarage.roles
CREATE TABLE IF NOT EXISTS `roles` (
                                       `role_id` int(11) NOT NULL AUTO_INCREMENT,
                                       `role` varchar(16) NOT NULL,
                                       PRIMARY KEY (`role_id`),
                                       KEY `employees_users_user_id_fk` (`role`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table smartgarage.roles: ~2 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`role_id`, `role`) VALUES
                                            (1, 'Customer'),
                                            (2, 'Employee');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping structure for table smartgarage.carServices
CREATE TABLE IF NOT EXISTS `services` (
                                              `service_id` int(11) NOT NULL AUTO_INCREMENT,
                                              `name` varchar(64) NOT NULL,
                                              `price` double NOT NULL,
                                              PRIMARY KEY (`service_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Dumping data for table smartgarage.carServices: ~10 rows (approximately)
/*!40000 ALTER TABLE services DISABLE KEYS */;
INSERT INTO services (service_id, `name`, `price`)VALUES
                                                              (1, 'Oil change', 45),
                                                              (2, 'Tyre change', 67),
                                                              (3, 'Timing belt kit replacement', 174),
                                                              (4, 'Water pump replacement', 92),
                                                              (5, 'Fuel filter', 37),
                                                              (6, 'Cabin air filter', 32),
                                                              (7, 'Engine air filter', 33),
                                                              (8, 'Spark plugs change', 27),
                                                              (9, 'Coolant replacement', 112),
                                                              (10, 'Brake fluid replacement', 114);
/*!40000 ALTER TABLE services ENABLE KEYS */;

-- Dumping structure for table smartgarage.tokens
CREATE TABLE IF NOT EXISTS `tokens` (
                                        `token_id` int(11) NOT NULL AUTO_INCREMENT,
                                        `token` varchar(60) NOT NULL,
                                        `user_id` int(11) NOT NULL,
                                        `expiry_date` date NOT NULL,
                                        PRIMARY KEY (`token_id`),
                                        KEY `tokens_users_user_id_fk` (`user_id`),
                                        CONSTRAINT `tokens_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table smartgarage.tokens: ~0 rows (approximately)
/*!40000 ALTER TABLE `tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `tokens` ENABLE KEYS */;

-- Dumping structure for table smartgarage.users
CREATE TABLE IF NOT EXISTS `users` (
                                       `user_id` int(11) NOT NULL AUTO_INCREMENT,
                                       `username` varchar(32) NOT NULL,
                                       `first_name` varchar(32) ,
                                       `last_name` varchar(32) ,
                                       `phone` varchar(32) DEFAULT NULL,
                                       `password` char(60) NOT NULL,
                                       `email` varchar(32) DEFAULT NULL,
                                       `currency` enum('BGN','EUR','USD') NOT NULL DEFAULT 'BGN',
                                       `enabled` tinyint(1) NOT NULL,
                                       PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table smartgarage.users: ~5 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`user_id`, `username`, `first_name`, `last_name`, `phone`, `password`, `email`, `currency`, `enabled`) VALUES
                                                                                                                                (1, 'cdimitrov', 'Chavdar', 'Dimitrov', '0885810921', '$2a$12$mlbxQAP11NEvqwRsI9JtW.JEGe6yluqHqK1NzsUUNq0AiRfpQryLG', 'chavdardim@gmail.com', 'BGN', 1),
                                                                                                                                (2, 'istavrev', 'Ivaylo', 'Stavrev', '0855233421', '$2a$12$FJuWGnVHmna0t3TcNKIoIOj8HKJMzWGcPVB6BJe8mKeHOhjTSxM9W', 'litosarm@gmail.com', 'BGN', 1),
                                                                                                                                (3, 'v.valchev', 'Victor', 'Valchev', '0888111111', '$2a$12$46AjGUD49c0byd1tq1fP9Onpr3o6mAsgNAIvtY4UuaREVEDR.gXTO', 'victor@abv.bg', 'BGN', 1),
                                                                                                                                (4, 'vlad', 'Vladimir', 'Venkov', '0888222222', '$2a$12$k.Be88ypF8HNaEX3cJwfm.yd2/ZlnYhC/QYHJTDS6G1DeTomSW47S', 'vladi@gmail.com', 'BGN', 1),
                                                                                                                                (5, 'idimiev', 'Ivo', 'Dimiev', '0899333333', '$2a$12$yXmrA7gZEyiaRKffkm9//OXcC4TlKSFnxPphAHBNREpGxT1JLKAYa', 'ivo_dimiev@telerik.com', 'BGN', 1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table smartgarage.users_roles
CREATE TABLE IF NOT EXISTS `users_roles` (
                                             `user_id` int(11) DEFAULT NULL,
                                             `role_id` int(11) DEFAULT NULL,
                                             KEY `users_roles_roles_role_id_fk` (`role_id`),
                                             KEY `users_roles_users_user_id_fk` (`user_id`),
                                             CONSTRAINT `users_roles_roles_role_id_fk` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`),
                                             CONSTRAINT `users_roles_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table smartgarage.users_roles: ~5 rows (approximately)
/*!40000 ALTER TABLE `users_roles` DISABLE KEYS */;
INSERT INTO `users_roles` (`user_id`, `role_id`) VALUES
                                                     (1, 2),
                                                     (2, 1),
                                                     (3, 1),
                                                     (4, 1),
                                                     (5, 1);
/*!40000 ALTER TABLE `users_roles` ENABLE KEYS */;

-- Dumping structure for table smartgarage.visits
CREATE TABLE IF NOT EXISTS `visits` (
                                        `visit_id` int(11) NOT NULL AUTO_INCREMENT,
                                        `check_in` datetime NOT NULL,
                                        `check_out` datetime DEFAULT NULL,
                                        `car_id` int(11) NOT NULL,
                                        `user_id` int(11) DEFAULT NULL,
                                        PRIMARY KEY (`visit_id`),
                                        KEY `visits_cars_car_id_fk` (`car_id`),
                                        KEY `visits_users_user_id_fk` (`user_id`),
                                        CONSTRAINT `visits_cars_car_id_fk` FOREIGN KEY (`car_id`) REFERENCES `cars` (`car_id`),
                                        CONSTRAINT `visits_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table smartgarage.visits: ~2 rows (approximately)
/*!40000 ALTER TABLE `visits` DISABLE KEYS */;
INSERT INTO `visits` (`visit_id`, `check_in`, `check_out`, `car_id`, `user_id`) VALUES
                                                                                    (1, '2022-03-02 13:17:07', '2022-03-04 13:17:18', 2, 2),
                                                                                    (2, '2022-03-01 13:17:07', '2022-03-03 13:17:38', 1, 1);
/*!40000 ALTER TABLE `visits` ENABLE KEYS */;

-- Dumping structure for table smartgarage.visits_services
CREATE TABLE IF NOT EXISTS `visits_services` (
                                                 `visit_id` int(11) DEFAULT NULL,
                                                 `service_id` int(11) DEFAULT NULL,
                                                 KEY `cars_services_services_service_id_fk` (`service_id`),
                                                 KEY `visits_services_visits_visit_id_fk` (`visit_id`),
                                                 CONSTRAINT `cars_services_services_service_id_fk` FOREIGN KEY (`service_id`) REFERENCES services (service_id),
                                                 CONSTRAINT `visits_services_visits_visit_id_fk` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`visit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table smartgarage.visits_services: ~8 rows (approximately)
/*!40000 ALTER TABLE `visits_services` DISABLE KEYS */;
INSERT INTO `visits_services` (`visit_id`, `service_id`) VALUES
                                                             (2, 1),
                                                             (2, 2),
                                                             (1, 1),
                                                             (1, 3),
                                                             (1, 4),
                                                             (1, 5),
                                                             (1, 6),
                                                             (1, 7);
/*!40000 ALTER TABLE `visits_services` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
