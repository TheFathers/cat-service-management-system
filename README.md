# Car Service Management System

### System Requirements

Make sure you have [Docker](https://docs.docker.com) and [Docker Compose](https://docs.docker.com/compose) installed.

#### Deployment Steps
1. Clone or download the repository
```
git clone https://gitlab.com/TheFathers/car-service-management-system.git
cd car-service-management-system
```
2. Run all services and wait a few seconds until everything is initialized:
```
docker-compose up
```
3. Your instance of SmartGarage CE v0.1.0 is now up and running
- You can open it at [`http://localhost:5054`](http://localhost:5054).