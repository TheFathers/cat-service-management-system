FROM gradle:7.3.3-jdk17 AS build
COPY src /home/app/src
COPY --chown=gradle:gradle . /home/app
WORKDIR /home/app
RUN gradle build --no-daemon
EXPOSE 5054
ENTRYPOINT ["java","-jar","/home/app/build/libs/smartgarage-0.1.0.jar"]