package com.smartgarage.services;

import com.smartgarage.exceptions.models.DuplicateEntityException;
import com.smartgarage.exceptions.models.EntityNotFoundException;
import com.smartgarage.exceptions.models.UnauthorizedOperationException;
import com.smartgarage.models.CarService;
import com.smartgarage.models.User;
import com.smartgarage.models.Visit;
import com.smartgarage.models.enums.ServiceSortOptions;
import com.smartgarage.repositories.contracts.CarServiceRepository;
import com.smartgarage.repositories.contracts.VisitRepository;
import com.smartgarage.services.contracts.CarServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.smartgarage.constant.UserImplConstant.*;

@Service
public class CarServiceServiceImpl implements CarServiceService {

    private final CarServiceRepository carServiceRepository;

    private final VisitRepository visitRepository;

    @Autowired
    public CarServiceServiceImpl(CarServiceRepository carServiceRepository, VisitRepository visitRepository) {
        this.carServiceRepository = carServiceRepository;
        this.visitRepository = visitRepository;
    }

    @Override
    public List<CarService> getAll() {
        return carServiceRepository.getAll();
    }

    @Override
    public List<CarService> filter(Optional<String> name,
                                   Optional<Double> minPrice,
                                   Optional<Double> maxPrice,
                                   Optional<ServiceSortOptions> sortOptions) {
        return carServiceRepository.filter(name, minPrice, maxPrice, sortOptions);
    }

    @Override
    public CarService getById(User loggedInUser, int serviceId) {
        //TODO check if user can make modifications
        return carServiceRepository.getById(serviceId);
    }

    @Override
    public List<CarService> getServicesByCarId(int carId) {
        return carServiceRepository.getServicesByCarId(carId);
    }

    @Override
    public void create(User performer, CarService newCarService) {
        if (!performer.isEmployee()) throw new UnauthorizedOperationException(NOT_EMPLOYEE_CREATE_SERVICE);

        boolean duplicateExists = true;
        try {
            carServiceRepository.getByName(newCarService.getName());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Service", "name", newCarService.getName());
        }

        carServiceRepository.create(newCarService);
    }

    @Override
    public void update(User performer, CarService carService) {
        if (!performer.isEmployee()) throw new UnauthorizedOperationException(NOT_EMPLOYEE_UPDATE_SERVICE);
        boolean duplicateExists = true;
        try {
            CarService existingService = carServiceRepository.getByName(carService.getName());
            if (existingService.getCarServiceId() == carService.getCarServiceId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Service", "name", carService.getName());
        }
        carServiceRepository.update(carService);
    }

    @Override
    public void delete(User loggedInUser, int serviceId) {
        if(!loggedInUser.isEmployee()) throw new UnauthorizedOperationException(NOT_EMPLOYEE_DELETE_SERVICE);
        CarService carService = getById(loggedInUser, serviceId);
        List<Visit> visits = getVisitsByCarServiceId(serviceId);
        if (!visits.isEmpty()) throw new UnauthorizedOperationException(SERVICE_WITH_VISITS_DELETE_ERROR);
        carServiceRepository.delete(carService);
    }

    @Override
    public CarService getByName(String selectedService) {
        return carServiceRepository.getByName(selectedService);
    }

    @Override
    public List<Visit> getVisitsByCarServiceId(int serviceId) {
        return visitRepository.getVisitsByCarServiceId(serviceId);
    }
}
