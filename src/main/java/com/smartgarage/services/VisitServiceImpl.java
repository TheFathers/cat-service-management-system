package com.smartgarage.services;

import com.posadskiy.currencyconverter.CurrencyConverter;
import com.posadskiy.currencyconverter.enums.Currency;
import com.smartgarage.exceptions.models.EntityNotFoundException;
import com.smartgarage.exceptions.models.UnauthorizedOperationException;
import com.smartgarage.models.Car;
import com.smartgarage.models.User;
import com.smartgarage.models.Visit;
import com.smartgarage.repositories.contracts.CarRepository;
import com.smartgarage.repositories.contracts.VisitRepository;
import com.smartgarage.services.contracts.PdfGenerateService;
import com.smartgarage.services.contracts.VisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static java.lang.String.format;

@Service
public class VisitServiceImpl implements VisitService {
    public static final String NOT_OWNER_OR_EMPLOYEE = "Only employees and owners are allowed to modify a visit!";
    public static final String NOT_EMPLOYEE = "Unauthorized access. Only employee allowed!";
    public static final String VISIT_DELETE_ERROR = "Checked out visit cannot be deleted!";

    private final VisitRepository visitRepository;
    private final CarRepository carRepository;
    private final CurrencyConverter currencyConverter;
    private final PdfGenerateService pdfGenerateService;

    @Autowired
    public VisitServiceImpl(VisitRepository visitRepository,
                            CarRepository carRepository, CurrencyConverter currencyConverter, PdfGenerateService pdfGenerateService) {
        this.visitRepository = visitRepository;
        this.carRepository = carRepository;
        this.currencyConverter = currencyConverter;
        this.pdfGenerateService = pdfGenerateService;
    }

    @Override
    public List<Visit> getAll(User performer) {
        throwIfNotEmployee(performer);
        return visitRepository.getAll();
    }

    @Override
    public List<Visit> getAll(Optional<String> search, User performer) {
        throwIfNotEmployee(performer);
        if (search.isEmpty()) {
            return getAll(performer);
        }
        return visitRepository.search(search.get());
    }

    @Override
    public Visit getById(int visitId, User loggedInUser) {
        List<Integer> ids = loggedInUser.getVisits()
                .stream()
                .map(Visit::getVisitId)
                .collect(Collectors.toList());
        throwIfNotSameUserOrEmployee(loggedInUser, visitId, ids);
        return visitRepository.getById(visitId);
    }

    @Override
    public List<Visit> getVisitsByCarId(int carId, User loggedInUser) {
        List<Integer> ids = loggedInUser.getCars()
                .stream()
                .map(Car::getCarId)
                .collect(Collectors.toList());
        throwIfNotSameUserOrEmployee(loggedInUser, carId, ids);
        return visitRepository.getVisitsByCarId(carId);
    }

    @Override
    public void createVisitForCarWithCarId(int carId, User loggedInUser) {
        List<Integer> ids = loggedInUser.getCars()
                .stream()
                .map(Car::getCarId)
                .collect(Collectors.toList());
        throwIfNotSameUserOrEmployee(loggedInUser, carId, ids);
        Car currentCar = carRepository.getById(carId);
        User owner = currentCar.getUser();
        Visit newVisit = new Visit(LocalDateTime.now(), currentCar, owner);
        visitRepository.create(newVisit);
    }

    @Override
    public List<Visit> getVisitsByCarServiceId(int serviceId) {
        return visitRepository.getVisitsByCarServiceId(serviceId);
    }

    @Override
    public void create(Visit newVisit, User performer) {
        throwIfNotEmployee(performer);
        visitRepository.create(newVisit);
    }

    @Override
    public void update(Visit visit, User performer) {
        throwIfNotEmployee(performer);
        visitRepository.update(visit);
    }

    @Override
    public void delete(Visit visit, User performer) {
        throwIfNotEmployee(performer);
        if (visit.getCheckOut() != null) throw new UnauthorizedOperationException(VISIT_DELETE_ERROR);
        visitRepository.delete(visit);
    }

    @Override
    public void checkOut(int visitId, User loggedInUser) {
        throwIfNotEmployee(loggedInUser);
        Visit visit = visitRepository.getById(visitId);
        visit.setCheckOut(LocalDateTime.now());
        visitRepository.update(visit);
    }

    @Override
    public double getExchangeRate(User loggedInUser, String currencyName, Visit visit) throws EntityNotFoundException{
        throwIfNotSameUserOrEmployee(loggedInUser, visit);
        Currency newCurrency = Arrays.stream(Currency.values())
                .filter(c -> c.name().equalsIgnoreCase(currencyName))
                .findFirst().orElseThrow(() -> new EntityNotFoundException("currency", "name", currencyName));
        return currencyConverter.rate(Currency.BGN, newCurrency);
    }

    @Override
    public String createPdf(Visit visit, User loggedInUser, Optional<Double> exchangeRate,
                            Optional<String> currency) throws UnauthorizedOperationException, IOException {
        throwIfNotSameUserOrEmployee(loggedInUser, visit);
        String licencePlate = visit.getCar().getLicensePlate();
        String owner = visit.getUser().getFirstName() + " " + visit.getUser().getLastName();
        String fileName = format("%s_%s_%s.pdf", licencePlate, owner, visit.showCheckInDate());
        return pdfGenerateService.generatePdfFile(visit, exchangeRate, currency, fileName);
    }

    private void throwIfNotSameUserOrEmployee(User loggedInUser, Visit visit) {
        if (loggedInUser.getUserId() != visit.getUser().getUserId() && !loggedInUser.isEmployee())
            throw new UnauthorizedOperationException(NOT_OWNER_OR_EMPLOYEE);
    }

    private void throwIfNotSameUserOrEmployee(User loggedInUser, int id, List<Integer> ids) {
        if (!loggedInUser.isEmployee() && !ids.contains(id)) {
            throw new UnauthorizedOperationException(NOT_OWNER_OR_EMPLOYEE);
        }
    }

    private void throwIfNotEmployee(User loggedInUser){
        if (!loggedInUser.isEmployee()) throw new UnauthorizedOperationException(NOT_EMPLOYEE);
    }
}
