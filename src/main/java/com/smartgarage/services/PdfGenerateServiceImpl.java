package com.smartgarage.services;

import com.lowagie.text.DocumentException;
import com.smartgarage.models.Visit;
import com.smartgarage.services.contracts.PdfGenerateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
@PropertySource("classpath:application.yml")
public class PdfGenerateServiceImpl implements PdfGenerateService {
    private final Logger logger = LoggerFactory.getLogger(PdfGenerateServiceImpl.class);

    private final TemplateEngine templateEngine;

    private final String pdfDirectory;

    private final String templateName;

    @Autowired
    public PdfGenerateServiceImpl(TemplateEngine templateEngine, Environment env) {
        this.templateEngine = templateEngine;
        pdfDirectory = env.getProperty("pdf.directory");
        templateName = env.getProperty("pdf.template.name");
    }

    @Override
    public String generatePdfFile(Visit visit, Optional<Double> exchangeRate, Optional<String> currency, String fileName)
    {
        Map<String, Object> data = new HashMap<>();
        data.put("visit", visit);
        currency.ifPresent(ex -> data.put("currency", ex));
        exchangeRate.ifPresent(c-> data.put("exchangeRate", c));
        Context context = new Context();
        context.setVariables(data);

        String htmlContent = templateEngine.process(templateName, context);
        String path = pdfDirectory + File.separator + fileName;
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(path);
            ITextRenderer renderer = new ITextRenderer();
            renderer.setDocumentFromString(htmlContent);
            renderer.layout();
            renderer.createPDF(fileOutputStream, false);
            renderer.finishPDF();
            fileOutputStream.close();
        } catch (DocumentException | IOException e) {
            logger.error(e.getMessage(), e);
        }
        return path;
    }
}
