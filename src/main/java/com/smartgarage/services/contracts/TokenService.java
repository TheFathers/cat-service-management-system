package com.smartgarage.services.contracts;

import com.smartgarage.models.User;
import com.smartgarage.models.tokens.Token;

public interface TokenService {

    void delete(int tokenId);

    Token getToken(String token);

    void createVerificationTokenForUser(User user, String token);
}
