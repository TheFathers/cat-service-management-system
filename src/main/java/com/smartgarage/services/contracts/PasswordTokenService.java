package com.smartgarage.services.contracts;

import com.smartgarage.models.User;
import com.smartgarage.models.tokens.PasswordToken;

public interface PasswordTokenService {

    void createPasswordTokenForUser(User user, String token);

    PasswordToken getPasswordToken(String token);

    void delete(int tokenId);
}
