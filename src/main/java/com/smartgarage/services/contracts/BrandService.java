package com.smartgarage.services.contracts;

import com.smartgarage.models.attributes.Brand;

import java.util.List;

public interface BrandService {

    void create(Brand newBrand);

    List<Brand> getAllBrands();

    Brand getByBrandName(String name);

    void update(Brand brand);

    void remove(String brandName);
}
