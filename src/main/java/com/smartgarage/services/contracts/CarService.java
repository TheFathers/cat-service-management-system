package com.smartgarage.services.contracts;

import com.smartgarage.models.Car;
import com.smartgarage.models.User;
import com.smartgarage.models.enums.CarSortOptions;

import java.util.List;
import java.util.Optional;

public interface CarService {

    List<Car> getAll(User loggedInUser);

    List<Car> searchFromMVC(Optional<String> search, User loggedInUser);

    List<Car> searchFromREST(Optional<String> search, User loggedInUser);

    Car getById(User loggedInUser, int carId);

    <V> Car getByField(User loggedInUser, String name, V value);

    List<Car> filter(Optional<String> brandName,
                     Optional<String> modelName,
                     Optional<Integer> year,
                     Optional<CarSortOptions> sortOptions);

    void create(User performer, Car target);

    void update(User performer, Car target);

    void delete(User loggedInUser, int carId);
}
