package com.smartgarage.services;

import com.smartgarage.models.User;
import com.smartgarage.models.tokens.Token;
import com.smartgarage.repositories.contracts.TokenRepository;
import com.smartgarage.services.contracts.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TokenServiceImpl implements TokenService {

    private final TokenRepository tokenRepository;

    @Autowired
    public TokenServiceImpl(TokenRepository tokenRepository) {
        this.tokenRepository = tokenRepository;
    }

    @Override
    public void delete(int tokenId) {
        Token token = tokenRepository.getById(tokenId);
        tokenRepository.delete(token);
    }

    @Override
    public Token getToken(final String token) {
        return tokenRepository.getByField("token", token);
    }

    @Override
    public void createVerificationTokenForUser(User user, String token) {
        Token myToken = new Token(token, user);
        myToken.setToken(token);
        myToken.setUser(user);
        tokenRepository.create(myToken);
    }
}
