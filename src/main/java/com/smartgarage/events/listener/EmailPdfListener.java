package com.smartgarage.events.listener;

import com.smartgarage.events.OnEmailPdfCompleteEvent;
import com.smartgarage.models.Visit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.nio.file.Paths;

import static java.lang.String.format;

@Component
@PropertySource("classpath:application.yml")
public class EmailPdfListener implements ApplicationListener<OnEmailPdfCompleteEvent> {

    public static final String SENDER = "carmanagementsystemapi@gmail.com";
    private final JavaMailSender mailSender;

    private final Environment env;

    @Autowired
    public EmailPdfListener(JavaMailSender mailSender, Environment env) {
        this.mailSender = mailSender;
        this.env = env;
    }


    @Override
    public void onApplicationEvent(final OnEmailPdfCompleteEvent event) {
        try {
            this.confirmPasswordReset(event);
        } catch (MessagingException | IOException e) {
            e.printStackTrace();
        }
    }

    private void confirmPasswordReset(final OnEmailPdfCompleteEvent event) throws MessagingException, IOException {
        final Visit visit = event.getVisit();
        final String pdfPath = event.getPdfPath();


        final MimeMessage emailToUser = constructEmailMessageToCustomer(visit, pdfPath);
        mailSender.send(emailToUser);
    }

    private MimeMessage constructEmailMessageToCustomer(Visit visit, String pdfPath)
            throws MessagingException {
        final String owner = visit.getUser().getFirstName() + " " + visit.getUser().getLastName();
        final String sender = env.getProperty("support.email");
        final String recipientAddress = visit.getUser().getEmail();
        final String subject = "PDF Visit report";
        final String message = format("Please find attached the report of your car with license plate %s.",
                visit.getCar().getLicensePlate());
        final String pdfName = format("%s_%s_%s.pdf", visit.getCar().getLicensePlate(), owner, visit.showCheckInDate());

        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
        helper.setTo(sender);
        helper.setFrom(recipientAddress);
        helper.setSubject(subject);
        helper.setText(message);
        helper.addAttachment(pdfName, new FileSystemResource(Paths.get(pdfPath)));
        return mimeMessage;
    }
}
