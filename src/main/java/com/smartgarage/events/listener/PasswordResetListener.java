package com.smartgarage.events.listener;

import com.smartgarage.models.User;
import com.smartgarage.events.OnPasswordResetCompleteEvent;
import com.smartgarage.services.contracts.PasswordTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class PasswordResetListener implements ApplicationListener<OnPasswordResetCompleteEvent> {

    private final PasswordTokenService passwordTokenService;

    private final JavaMailSender mailSender;

    private final Environment env;

    @Autowired
    public PasswordResetListener(PasswordTokenService passwordTokenService, JavaMailSender mailSender, Environment env) {
        this.passwordTokenService = passwordTokenService;
        this.mailSender = mailSender;
        this.env = env;
    }


    @Override
    public void onApplicationEvent(final OnPasswordResetCompleteEvent event) {
        this.confirmPasswordReset(event);
    }

    private void confirmPasswordReset(final OnPasswordResetCompleteEvent event) {
        final User user = event.getUser();
        final String passwordToken = UUID.randomUUID().toString();
        passwordTokenService.createPasswordTokenForUser(user, passwordToken);

        final SimpleMailMessage emailToNewUser = constructEmailMessageToNewUser(user, passwordToken, event.getPath());
        final SimpleMailMessage emailToEmployee = constructEmailMessageToEmployee(passwordToken, event.getPath());
        mailSender.send(emailToNewUser);
        mailSender.send(emailToEmployee);
    }

    private SimpleMailMessage constructEmailMessageToNewUser(final User user, final String token, String path) {
        final String recipientAddress = user.getEmail();
        final String subject = "Password reset email";
        final String confirmationUrl = "localhost:8080" + path + "?passwordToken=" + token;
        final String message = "To reset your password, please click on the below link:\n";

        final SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(recipientAddress);
        email.setSubject(subject);
        email.setText(message + " \r\n" + confirmationUrl);
        email.setFrom(env.getProperty("support.email"));
        return email;
    }

    private SimpleMailMessage constructEmailMessageToEmployee(final String token, String path) {
        final String recipientAddress = "carmanagementsystemapi@gmail.com";
        final String subject = "New user Password reset request";
        final String confirmationUrl = "localhost:8080" + path + "?passwordToken=" + token;
        final String message = " The following link for password reset was sent:\n";

        final SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(recipientAddress);
        email.setSubject(subject);
        email.setText(message + " \r\n" + confirmationUrl);
        email.setFrom(env.getProperty("support.email"));
        return email;
    }
    

}
