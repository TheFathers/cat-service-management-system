package com.smartgarage.events.listener;

import com.smartgarage.models.User;
import com.smartgarage.events.OnRegistrationCompleteEvent;
import com.smartgarage.services.contracts.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class RegistrationListener implements ApplicationListener<OnRegistrationCompleteEvent> {

    private final TokenService tokenService;

    private final JavaMailSender mailSender;

    private final Environment env;

    @Autowired
    public RegistrationListener(TokenService tokenService, JavaMailSender mailSender, Environment env) {
        this.tokenService = tokenService;
        this.mailSender = mailSender;
        this.env = env;
    }


    @Override
    public void onApplicationEvent(final OnRegistrationCompleteEvent event) {
        this.confirmRegistration(event);
    }

    private void confirmRegistration(final OnRegistrationCompleteEvent event) {
        final User user = event.getUser();
        final String registerType = event.getRegisterType();
        final String token = UUID.randomUUID().toString();
        tokenService.createVerificationTokenForUser(user, token);

        final SimpleMailMessage emailToNewUser = constructEmailMessageToNewUser(user, token, registerType);
        final SimpleMailMessage emailToEmployee = constructEmailMessageToEmployee(token, registerType);
        mailSender.send(emailToNewUser);
        mailSender.send(emailToEmployee);
    }

    private SimpleMailMessage constructEmailMessageToNewUser(final User user, final String token, String registerType) {
        final String recipientAddress = user.getEmail();
        final String subject = "Welcome to Pit Stop Service";
        final String confirmationUrl = "localhost:8080/" + registerType + "/register/confirm?token=" + token;
        final String message = "Your account was created successfully. To confirm your registration, please click on the below link:\n";

        final SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(recipientAddress);
        email.setSubject(subject);
        email.setText(message + " \r\n" + confirmationUrl);
        email.setFrom(env.getProperty("support.email"));
        return email;
    }

    private SimpleMailMessage constructEmailMessageToEmployee(final String token, String registerType) {
        final String recipientAddress = "carmanagementsystemapi@gmail.com";
        final String subject =  "New " + registerType + " Registration Confirmation";
        final String confirmationUrl = "localhost:8080/" + registerType + "/register/confirm?token=" + token;
        final String message = "User account was created successfully. The following link for confirmation was sent to him:\n";

        final SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(recipientAddress);
        email.setSubject(subject);
        email.setText(message + " \r\n" + confirmationUrl);
        email.setFrom(env.getProperty("support.email"));
        return email;
    }
    

}
