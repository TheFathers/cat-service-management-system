package com.smartgarage.events;

import com.smartgarage.models.User;
import com.smartgarage.models.Visit;
import org.springframework.context.ApplicationEvent;

public class OnEmailPdfCompleteEvent extends ApplicationEvent {

    private final Visit visit;

    private final String pdfPath;

    public OnEmailPdfCompleteEvent(Visit visit, String pdfPath) {
        super(visit.getUser());
        this.visit = visit;
        this.pdfPath = pdfPath;
    }

    public Visit getVisit() {
        return visit;
    }

    public String getPdfPath() {
        return pdfPath;
    }
}
