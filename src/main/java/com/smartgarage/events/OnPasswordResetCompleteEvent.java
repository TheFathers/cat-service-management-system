package com.smartgarage.events;

import com.smartgarage.models.User;
import org.springframework.context.ApplicationEvent;

public class OnPasswordResetCompleteEvent extends ApplicationEvent {

    private final User user;

    private final String path;

    public OnPasswordResetCompleteEvent(User user, String path) {
        super(user);
        this.user = user;
        this.path =  path;
    }

    public User getUser() {
        return user;
    }

    public String getPath() {
        return path;
    }
}
