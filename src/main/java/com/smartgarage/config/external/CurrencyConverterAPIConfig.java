package com.smartgarage.config.external;

import com.posadskiy.currencyconverter.CurrencyConverter;
import com.posadskiy.currencyconverter.config.ConfigBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:application.yml")
public class CurrencyConverterAPIConfig {

    private final String currencyConverterApiKey;
    private final String currencyLayerApiKey;
    private final String openExchangeRatesApiKey;

    @Autowired
    public CurrencyConverterAPIConfig(Environment env) {
        currencyConverterApiKey = env.getProperty("currency.converter.api.key");
        currencyLayerApiKey = env.getProperty("currency.layer.api.key");
        openExchangeRatesApiKey = env.getProperty("currency.open.exchange.rates.api.key");
    }

    @Bean(name="currencyConverter")
    public CurrencyConverter getCurrencyConverter(){
        return new CurrencyConverter(
                new ConfigBuilder()
                        .currencyConverterApiApiKey(currencyConverterApiKey)
                        .currencyLayerApiKey(currencyLayerApiKey)
                        .openExchangeRatesApiKey(openExchangeRatesApiKey)
                        .build()
        );
    }
}
