package com.smartgarage.controllers.mvc;

import com.smartgarage.exceptions.models.EntityNotFoundException;
import com.smartgarage.models.Role;
import com.smartgarage.models.User;
import com.smartgarage.models.attributes.Brand;
import com.smartgarage.models.attributes.CarModel;
import com.smartgarage.models.dto.BrandAndModelDTO;
import com.smartgarage.models.dto.BrandDTO;
import com.smartgarage.models.dto.ModelDTO;
import com.smartgarage.services.contracts.BrandService;
import com.smartgarage.services.contracts.ModelService;
import com.smartgarage.utils.helper.AuthenticationHelper;
import com.smartgarage.utils.mapper.BrandAndModelMapper;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/brands")
public class BrandsAndModelsMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final BrandService brandService;
    private final BrandAndModelMapper brandAndModelMapper;
    private final ModelService modelService;

    public BrandsAndModelsMvcController(AuthenticationHelper authenticationHelper,
                                        BrandService brandService,
                                        BrandAndModelMapper brandAndModelMapper,
                                        ModelService modelService) {
        this.authenticationHelper = authenticationHelper;
        this.brandService = brandService;
        this.brandAndModelMapper = brandAndModelMapper;
        this.modelService = modelService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated() {
        return SecurityContextHolder.getContext().getAuthentication().getName() != null;
    }

    @ModelAttribute("isEmployee")
    public boolean isEmployee() {
        boolean isEmployee = false;
        Role employeeRole = new Role();
        employeeRole.setRole("Employee");
        employeeRole.setRoleId(2);
        if (populateIsAuthenticated())
            isEmployee = authenticationHelper.tryGetUser().getRoles().contains(employeeRole);
        return isEmployee;
    }

    @ModelAttribute("loggedInUser")
    public User getLoggedInUser() {
        if (populateIsAuthenticated()) return authenticationHelper.tryGetUser();
        return null;
    }

    @ModelAttribute("modelsNotInUse")
    public List<String> getModelsNotInUse() {
        return modelService.getModelsNotInUse();
    }

    @ModelAttribute("models")
    public List<CarModel> getAllModels() {
        return modelService.getAllModels();
    }

    @ModelAttribute("brands")
    public List<Brand> getAllBrands() {
        return brandService.getAllBrands();
    }

    @GetMapping
    public String getAllBrands(Model model) {
        model.addAttribute("brandModelDto", new BrandAndModelDTO());
        model.addAttribute("brandDto", new BrandDTO());
        return "brands/brands";
    }

    @PostMapping("/create")
    public String createBrandOrModel(Model model,
                                     RedirectAttributes redirectAttributes,
                                     @Valid @ModelAttribute("brandModelDto") BrandAndModelDTO brandAndModelDTO,
                                     BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "/brands/brands";
        }
        try {
            Brand brand = brandAndModelMapper.createBrandFromDto(brandAndModelDTO);
            brandService.create(brand);
            redirectAttributes.addAttribute("success", "Operation Successful");
        } catch (EntityNotFoundException e) {
            redirectAttributes.addAttribute("error", e.getMessage());
        }
        return "redirect:/brands";
    }

    @PostMapping("/delete/brand")
    public String deleteBrand(Model model,
                              RedirectAttributes redirectAttributes,
                              @RequestParam("brandName") String brandName) {

        try {
            brandService.remove(brandName);
            redirectAttributes.addAttribute("success", "Brand Deactivate is Successful");
        } catch (EntityNotFoundException e) {
            redirectAttributes.addAttribute("error", e.getMessage());
        }
        return "redirect:/brands";
    }

    @PostMapping("/delete/model")
    public String deleteModel(Model model,
                              RedirectAttributes redirectAttributes,
                              @RequestParam("modelName") String modelName) {

        try {
            modelService.remove(modelName);
            redirectAttributes.addAttribute("success", "Model Deactivate is Successful");
        } catch (EntityNotFoundException e) {
            redirectAttributes.addAttribute("error", e.getMessage());
        }
        return "redirect:/brands";
    }
}
