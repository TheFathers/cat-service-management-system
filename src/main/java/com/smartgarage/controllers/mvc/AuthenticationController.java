package com.smartgarage.controllers.mvc;

import com.smartgarage.exceptions.models.EntityNotFoundException;
import com.smartgarage.models.User;
import com.smartgarage.models.dto.EmailDTO;
import com.smartgarage.models.dto.PasswordDTO;
import com.smartgarage.models.tokens.PasswordToken;
import com.smartgarage.events.OnPasswordResetCompleteEvent;
import com.smartgarage.services.contracts.PasswordTokenService;
import com.smartgarage.services.contracts.UserService;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.WebAttributes;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Calendar;

import static com.smartgarage.constant.UserImplConstant.USER_IS_DISABLED;
import static java.lang.String.format;

@Controller
@RequestMapping
public class AuthenticationController {

    private final UserService userService;

    private final PasswordEncoder passwordEncoder;

    private final PasswordTokenService passwordTokenService;

    private final ApplicationEventPublisher eventPublisher;

    public AuthenticationController(UserService userService, PasswordEncoder passwordEncoder, PasswordTokenService passwordTokenService, ApplicationEventPublisher eventPublisher) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
        this.passwordTokenService = passwordTokenService;
        this.eventPublisher = eventPublisher;
    }

    @GetMapping("/login")
    public String login(){
        return "login";
    }

    @GetMapping("/login-error")
    public String login(HttpServletRequest request, Model model) {
        HttpSession session = request.getSession(false);
        String errorMessage = null;
        if (session != null) {
            AuthenticationException ex = (AuthenticationException) session
                    .getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
            if (ex != null) {
                errorMessage = ex.getMessage();
            }
        }
        model.addAttribute("error", errorMessage);
        return "login";
    }

    @GetMapping("/password/forget")
    public String requestNewPassword(Model model){
        model.addAttribute("email", new EmailDTO());
        return "/authentication/password-forget";
    }

    @PostMapping("/password/forget")
    public String requestNewPassword(Model model,
                                     @Valid @ModelAttribute("email") EmailDTO emailDTO,
                                     BindingResult bindingResult){
        if (bindingResult.hasErrors()) {
            return "/authentication/password-forget";
        }
        try {
            User user = userService.getByField("email", emailDTO.getEmail());
            if (!user.isEnabled())
                bindingResult.rejectValue("email", "user_not_enabled", USER_IS_DISABLED);
            eventPublisher.publishEvent(new OnPasswordResetCompleteEvent(user, "/password/reset"));
            model.addAttribute("message", format("An email was sent to %s.", emailDTO.getEmail()));
            return "/authentication/password-forget";
        }  catch (EntityNotFoundException e) {
            bindingResult.rejectValue("email", "email_not_found", e.getMessage());
            return "/authentication/password-forget";
        }
    }

    @PostMapping("/password/token")
    public String employeeSendNewPasswordToCustomer(
                                        RedirectAttributes redirectAttributes,
                                     @RequestParam("email") String email){

            User user = userService.getByField("email", email);
            if (!user.isEnabled()){
                redirectAttributes.addAttribute("error", USER_IS_DISABLED);
                return "redirect:/customers";
            }
            eventPublisher.publishEvent(new OnPasswordResetCompleteEvent(user, "/password/reset"));
            redirectAttributes.addAttribute
                    ("success", "Successful password reset email sent to " + user.getEmail());
            return "redirect:/customers";
    }


    @GetMapping("/password/reset")
    public String resetPassword
            (Model model,
             @RequestParam("passwordToken") String token) {

        if (token == null) {
            model.addAttribute("message", "Invalid token.");
            return "/authentication/bad-user";
        }
        PasswordToken passwordToken;
        try {
            passwordToken = passwordTokenService.getPasswordToken(token);
        } catch (EntityNotFoundException e) {
            model.addAttribute("message", "Invalid token.");
            return "/authentication/bad-user";
        }
        Calendar cal = Calendar.getInstance();
        if ((passwordToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
            model.addAttribute("expired", true);
            model.addAttribute("token", passwordToken);
            model.addAttribute("message", "Your registration token has expired. Please register again.");
            passwordTokenService.delete(passwordToken.getPasswordTokenId());
            return "/authentication/bad-user";
        }
        PasswordDTO passwordDTO = new PasswordDTO();
        passwordDTO.setPasswordToken(token);
        model.addAttribute("password", passwordDTO);
        return "/authentication/password-reset";
    }

    @PostMapping("/password/reset")
    public String changePassword (
            RedirectAttributes redirectAttributes,
            Model model,
             @Valid @ModelAttribute("password") PasswordDTO passwordDTO,
             BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "/authentication/password-reset";
        }
        try {
            PasswordToken passwordToken = passwordTokenService.getPasswordToken(passwordDTO.getPasswordToken());
            String encryptedPassword = passwordEncoder.encode(passwordDTO.getPassword());
            userService.passwordReset(passwordToken.getUser(), encryptedPassword);
            passwordTokenService.delete(passwordToken.getPasswordTokenId());
            redirectAttributes.addAttribute("success", "Password changed!");
            return "redirect:/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("message", "Invalid token.");
            return "/authentication/bad-user";
        }
    }
    @GetMapping("/bad-user")
    public String badUser(){
        return "/authentication/bad-user";
    }
}
