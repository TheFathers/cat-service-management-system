package com.smartgarage.controllers.mvc;

import com.smartgarage.exceptions.models.DuplicateEntityException;
import com.smartgarage.exceptions.models.EntityNotFoundException;
import com.smartgarage.exceptions.models.UnauthorizedOperationException;
import com.smartgarage.models.User;
import com.smartgarage.models.dto.CustomerDTO;
import com.smartgarage.models.dto.RegisterDTO;
import com.smartgarage.models.tokens.Token;
import com.smartgarage.events.OnRegistrationCompleteEvent;
import com.smartgarage.services.contracts.TokenService;
import com.smartgarage.services.contracts.UserService;
import com.smartgarage.utils.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Calendar;

import static com.smartgarage.constant.UserImplConstant.CUSTOMER;

@Controller
public class RegisterCustomerController {

    private final UserService userService;
    private final TokenService tokenService;
    private final UserMapper userMapper;
    private final ApplicationEventPublisher eventPublisher;

    @Autowired
    public RegisterCustomerController(UserService userService,
                                      TokenService tokenService, UserMapper userMapper,
                                      ApplicationEventPublisher eventPublisher) {
        this.userService = userService;
        this.tokenService = tokenService;
        this.userMapper = userMapper;
        this.eventPublisher = eventPublisher;
    }

    @GetMapping("/customer/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("register", new CustomerDTO());
        return "user/customer-register";
    }

    @PostMapping("/customer/register")
    public String handleRegister(RedirectAttributes redirectAttributes,
                                @Valid @ModelAttribute("register") CustomerDTO customerDTO,
                                 BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "user/customer-register";
        }
        try {
            User user = userMapper.customerDtoToUserCreate(customerDTO);
            userService.create(user);
            User registered = userService.getByField("email", customerDTO.getEmail());
            eventPublisher.publishEvent(new OnRegistrationCompleteEvent(registered, CUSTOMER));
            redirectAttributes.addAttribute("success", "Successfully customer register.");
            return "redirect:/customers";
        } catch (DuplicateEntityException e) {
            bindErrorByField(bindingResult, e, "userName", "duplicate_username_error");
            bindErrorByField(bindingResult, e, "email", "duplicate_email_error");
            bindErrorByField(bindingResult, e, "phone", "duplicate_phone_error");
            return "user/customer-register";
        } catch (UnauthorizedOperationException e) {
            bindingResult.rejectValue("userType", "usertype_role", e.getMessage());
            return "user/customer-register";
        }
    }

    @GetMapping("/customer/register/confirm")
    public String confirmRegistration
            (Model model,
             @RequestParam("token") String token) {

        if (token == null) {
            model.addAttribute("message", "Invalid token.");
            return "/authentication/bad-user";
        }
        Token verificationToken;
        try {
            verificationToken = tokenService.getToken(token);
        } catch (EntityNotFoundException e) {
            model.addAttribute("message", "Invalid token.");
            return "/authentication/bad-user";
        }

        User user = verificationToken.getUser();
        Calendar cal = Calendar.getInstance();
        if ((verificationToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
            model.addAttribute("expired", true);
            model.addAttribute("token", token);
            model.addAttribute("message", "Your registration token has expired. Please register again.");
            tokenService.delete(verificationToken.getTokenId());
            return "/authentication/bad-user";
        }
        RegisterDTO registerDTO = userMapper.userToRegisterDTO(user, token);
        model.addAttribute("register", registerDTO);
        return "user/customer-confirm";
    }

    @PostMapping("/customer/register/confirm")
    public String handleConfirmation
            (RedirectAttributes redirectAttributes,
             Model model,
             @Valid @ModelAttribute("register") RegisterDTO registerDTO,
             BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "user/customer-confirm";
        }
        try {
            Token verificationToken = tokenService.getToken(registerDTO.getToken());
            User user = userMapper.registerDtoToUserUpdate(registerDTO, verificationToken.getUser());
            user.setEnabled(true);
            userService.update(user, user);
            tokenService.delete(verificationToken.getTokenId());
            redirectAttributes.addAttribute("success", "Account verified!");
            return "redirect:/login";
        } catch (DuplicateEntityException e) {
            bindErrorByField(bindingResult, e, "userName", "duplicate_username_error");
            bindErrorByField(bindingResult, e, "email", "duplicate_email_error");
            bindErrorByField(bindingResult, e, "phone", "duplicate_phone_error");
            return "user/customer-confirm";
        } catch (EntityNotFoundException e) {
            model.addAttribute("message", "Invalid token.");
            return "/authentication/bad-user";
        }
    }

    private void bindErrorByField(BindingResult bindingResult,
                                  DuplicateEntityException e,
                                  String field,
                                  String errorCode) {
        if (e.getMessage().contains(field)) bindingResult.rejectValue(field, errorCode, e.getMessage());
    }
}
