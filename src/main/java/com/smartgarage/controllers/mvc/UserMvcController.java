package com.smartgarage.controllers.mvc;

import com.smartgarage.exceptions.models.DuplicateEntityException;
import com.smartgarage.exceptions.models.EntityNotFoundException;
import com.smartgarage.exceptions.models.UnauthorizedOperationException;
import com.smartgarage.models.User;
import com.smartgarage.models.dto.PasswordDTO;
import com.smartgarage.models.dto.UserDTO;
import com.smartgarage.models.dto.UserOutDTO;
import com.smartgarage.events.OnRegistrationCompleteEvent;
import com.smartgarage.services.contracts.UserService;
import com.smartgarage.utils.helper.AuthenticationHelper;
import com.smartgarage.utils.mapper.UserMapper;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

import static com.smartgarage.constant.UserImplConstant.CUSTOMER;

@Controller
@RequestMapping("/customers")
public class UserMvcController {

    private final UserService userService;
    private final PasswordEncoder passwordEncoder;
    private final AuthenticationHelper authenticationHelper;
    private final UserMapper userMapper;
    private final ApplicationEventPublisher eventPublisher;

    public UserMvcController(UserService userService,
                             PasswordEncoder passwordEncoder,
                             AuthenticationHelper authenticationHelper,
                             UserMapper userMapper, ApplicationEventPublisher eventPublisher) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
        this.authenticationHelper = authenticationHelper;
        this.userMapper = userMapper;
        this.eventPublisher = eventPublisher;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated() {
        return SecurityContextHolder.getContext().getAuthentication().getName() != null;
    }

    @ModelAttribute("isEmployee")
    public boolean isEmployee() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return auth.getAuthorities()
                .stream()
                .anyMatch(a -> a.getAuthority().equals("ROLE_Employee"));
    }

    @ModelAttribute("loggedInUser")
    public User getLoggedInUser() {
        if (populateIsAuthenticated()) return authenticationHelper.tryGetUser();
        return null;
    }

    @GetMapping
    public String showAllUsers(RedirectAttributes redirectAttributes,
                               Model model) {

        User loggedInUser;
        try {
            loggedInUser = authenticationHelper.tryGetUser();
            model.addAttribute("customers", userService.getAllCustomers(loggedInUser));
            return "user/customers";
        } catch (UnauthorizedOperationException e) {
            redirectAttributes.addAttribute("error", e.getMessage());
            return "redirect:/";
        }
    }

    @GetMapping("/{userId}")
    public String getById(RedirectAttributes redirectAttributes, @PathVariable int userId, Model model) {
        try {
            User loggedInUser = authenticationHelper.tryGetUser();
            UserOutDTO userOutDTO = userMapper.userToUserOutDto(loggedInUser);
            boolean isSameUser = loggedInUser.getUserId() == userId;
            model.addAttribute("profile", userService.getById(loggedInUser, userId));
            model.addAttribute("profileEdit", userOutDTO);
            model.addAttribute("passwordChange", new PasswordDTO());
            model.addAttribute("isSameUser", isSameUser);
            return "user/user-profile";
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            redirectAttributes.addAttribute("error", e.getMessage());
            return "redirect:/customers/{userId}";
        }
    }

    @PostMapping("/{userId}/enable")
    public String enable(RedirectAttributes redirectAttributes,
                          @PathVariable int userId) {
        try {
            User loggedInUser = authenticationHelper.tryGetUser();
            String username = userService.enable(loggedInUser, userId);
            redirectAttributes.addAttribute
                    ("success", "Successful enable of customer " + username);
            return "redirect:/customers";
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            redirectAttributes.addAttribute("error", e.getMessage());
            return "redirect:/customers";
        }
    }

    @PostMapping("/{userId}/disable")
    public String disable(RedirectAttributes redirectAttributes,
                         @PathVariable int userId) {
        try {
            User loggedInUser = authenticationHelper.tryGetUser();
            String username = userService.disable(loggedInUser, userId);
            redirectAttributes.addAttribute
                    ("success", "Successful disable of customer " + username);
            return "redirect:/customers";
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            redirectAttributes.addAttribute("error", e.getMessage());
            return "redirect:/customers";
        }
    }

    @PostMapping("/{userId}/new/token")
    public String sendNewToken(RedirectAttributes redirectAttributes,
                               @PathVariable int userId) {
        try {
            User loggedInUser = authenticationHelper.tryGetUser();
            User registered = userService.getById(loggedInUser, userId);
            eventPublisher.publishEvent(new OnRegistrationCompleteEvent(registered, CUSTOMER));
            redirectAttributes.addAttribute
                    ("success", "New token was sent to customer with email: " + registered.getEmail());
            return "redirect:/customers";
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            redirectAttributes.addAttribute("error", e.getMessage());
            return "redirect:/customers";
        }
    }

    @PostMapping("/{userId}/update")
    public String updateUser(RedirectAttributes redirectAttributes,
                             @PathVariable int userId,
                             @Valid @ModelAttribute("profileEdit") UserDTO userDTO,
                             BindingResult errors) {


        if (errors.hasErrors()) {
            List<String> errorMessages = errors.getAllErrors()
                    .stream()
                    .map(ObjectError::getDefaultMessage)
                    .collect(Collectors.toList());
            redirectAttributes.addAttribute("error", String.join("\n", errorMessages));
            return "redirect:/customers/{userId}";
        }
        try {
            User loggedInUser = authenticationHelper.tryGetUser();
            User user = userService.getById(loggedInUser, userId);
            User userToBeUpdated = userMapper.userDtoToUserUpdate(loggedInUser, userDTO, user);
            userService.update(userToBeUpdated, loggedInUser);
            if (loggedInUser.getUserId() == userId) {
                Authentication authentication = new UsernamePasswordAuthenticationToken(loggedInUser,
                        loggedInUser.getPassword(), loggedInUser.getAuthorities());
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
            redirectAttributes.addAttribute("success", "Successful profile update");
            return "redirect:/customers/{userId}";
        } catch (DuplicateEntityException | UnauthorizedOperationException e)  {
            redirectAttributes.addAttribute("error", e.getMessage());
            return "redirect:/customers/{userId}";
        }
    }

    @PostMapping("/{userId}/new/password")
    public String changePassword(RedirectAttributes redirectAttributes,
                                @PathVariable int userId,
                             @ModelAttribute("profile") UserDTO userDTO,
                             @Valid @ModelAttribute ("passwordChange") PasswordDTO passwordDTO,
                             BindingResult errors) {
        if (errors.hasErrors()) {
            List<String> errorMessages = errors.getAllErrors()
                    .stream()
                    .map(ObjectError::getDefaultMessage)
                    .collect(Collectors.toList());
            redirectAttributes.addAttribute("error", String.join("\n", errorMessages));
            return "redirect:/customers/{userId}";
        }
        try {
            User loggedInUser = authenticationHelper.tryGetUser();
            String encryptedPassword = passwordEncoder.encode(passwordDTO.getPassword());
            userService.updateUserPassword(loggedInUser, userId, encryptedPassword);
            redirectAttributes.addAttribute("success", "Successful password change");
            return "redirect:/customers/{userId}";
        } catch (UnauthorizedOperationException e) {
            redirectAttributes.addAttribute("error", e.getMessage());
            return "redirect:/customers/{userId}";
        }
    }

}


