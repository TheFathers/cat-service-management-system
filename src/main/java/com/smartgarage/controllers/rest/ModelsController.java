package com.smartgarage.controllers.rest;

import com.smartgarage.models.attributes.CarModel;
import com.smartgarage.services.contracts.ModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/models")
public class ModelsController {

    private final ModelService modelService;

    @Autowired
    public ModelsController(ModelService modelService) {
        this.modelService = modelService;
    }


    @GetMapping
    public List<CarModel> getAllModels(@RequestParam(value = "search", required = false) Optional<String> search) {

        if (search.isEmpty()) {
            return modelService.getAllModels();
        }

        return modelService.getAllModels().stream()
                .filter(model -> model.getName()
                        .toLowerCase()
                        .contains(search.get().toLowerCase()))
                .limit(10)
                .collect(Collectors.toList());
    }

}

