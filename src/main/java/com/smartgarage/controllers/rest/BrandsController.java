package com.smartgarage.controllers.rest;

import com.smartgarage.models.User;
import com.smartgarage.models.attributes.Brand;
import com.smartgarage.services.contracts.BrandService;
import com.smartgarage.utils.helper.AuthenticationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/brands")
public class BrandsController {

    private final BrandService brandService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public BrandsController(BrandService brandService,
                            AuthenticationHelper authenticationHelper) {
        this.brandService = brandService;
        this.authenticationHelper = authenticationHelper;
    }


    @GetMapping
    public List<Brand> getAllBrands(@RequestParam(value = "search", required = false) Optional<String> search) {

        User loggedInUser = authenticationHelper.tryGetUser();
        if (search.isEmpty()) {
            return brandService.getAllBrands();
        }

        return brandService.getAllBrands().stream()
                .filter(brand -> brand.getName()
                        .toLowerCase()
                        .contains(search.get()))
                .collect(Collectors.toList());
    }

    @GetMapping("/names")
    public List<String> getAllBrandNames(@RequestParam(value = "search", required = false) Optional<String> search) {
        return getAllBrands(search).stream()
                .map(Brand::getName)
                .collect(Collectors.toList());
    }

}

