package com.smartgarage.controllers.rest;

import com.smartgarage.models.User;
import com.smartgarage.models.Visit;
import com.smartgarage.services.contracts.VisitService;
import com.smartgarage.utils.helper.AuthenticationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@RestController
@RequestMapping("api/visits")
public class VisitsController {

    private final VisitService visitService;

    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public VisitsController(VisitService visitService, AuthenticationHelper authenticationHelper) {
        this.visitService = visitService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Visit> getAllVisit(@RequestParam(value = "search", required = false) Optional<String> search) {
        User loggedInUser = authenticationHelper.tryGetUser();
        if (search.isEmpty()) {
            return visitService.getAll(loggedInUser);
        }
        return visitService.getAll(loggedInUser).stream()
                .filter(visit -> visit.getServices().stream()
                        .anyMatch(carService ->
                                carService.getName().toLowerCase().contains(search.get().toLowerCase())))
                .collect(Collectors.toList());
    }


}



