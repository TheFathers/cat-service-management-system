package com.smartgarage.controllers.rest;


import com.smartgarage.exceptions.models.DuplicateEntityException;
import com.smartgarage.exceptions.models.EntityNotFoundException;
import com.smartgarage.exceptions.models.UnauthorizedOperationException;
import com.smartgarage.models.Car;
import com.smartgarage.models.User;
import com.smartgarage.models.dto.*;
import com.smartgarage.models.enums.CarSortOptions;
import com.smartgarage.services.contracts.UserService;
import com.smartgarage.utils.helper.AuthenticationHelper;
import com.smartgarage.utils.mapper.CarMapper;
import com.smartgarage.utils.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/users")
public class UsersController {

    private final UserService service;
    private final AuthenticationHelper authenticationHelper;
    private final UserMapper mapper;


    @Autowired
    public UsersController(UserService service,
                           AuthenticationHelper authenticationHelper,
                           UserMapper mapper) {
        this.service = service;
        this.authenticationHelper = authenticationHelper;
        this.mapper = mapper;
    }

    // search by license plate, VIN or owner’s phone number
    @GetMapping
    public List<UserOutDTO> getAllUsers(@RequestParam(required = false) Optional<String> search) {
        User loggedInUser = authenticationHelper.tryGetUser();
        return service.getAll(search, loggedInUser).stream()
                .map(mapper::userToUserOutDto)
                .collect(Collectors.toList());
    }

    @PostMapping
    public User create(@Valid @RequestBody RegisterDTO registerDTO) {
        try {
            User loggedInUser = authenticationHelper.tryGetUser();
            User user = mapper.registerDtoToUserCreate(registerDTO);
            service.create(user);
            return user;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public User update(@PathVariable int id, @Valid @RequestBody UserDTO userDTO) {
        try {
            User loggedInUser = authenticationHelper.tryGetUser();
            User userToBeUpdated = service.getById(loggedInUser, id);
            User user = mapper.userDtoToUserUpdate(loggedInUser, userDTO, userToBeUpdated);
            service.update(userToBeUpdated, loggedInUser);
            return user;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            User loggedInUser = authenticationHelper.tryGetUser();
            service.delete(loggedInUser, id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }


}
