package com.smartgarage.controllers.rest;


import com.smartgarage.exceptions.models.DuplicateEntityException;
import com.smartgarage.exceptions.models.EntityNotFoundException;
import com.smartgarage.exceptions.models.UnauthorizedOperationException;
import com.smartgarage.models.Car;
import com.smartgarage.models.User;
import com.smartgarage.models.dto.CarDTO;
import com.smartgarage.models.dto.CarOutDTO;
import com.smartgarage.models.enums.CarSortOptions;
import com.smartgarage.services.contracts.CarService;
import com.smartgarage.utils.helper.AuthenticationHelper;
import com.smartgarage.utils.mapper.CarMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/cars")
public class CarsController {

    private final CarService service;
    private final AuthenticationHelper authenticationHelper;
    private final CarMapper carMapper;


    @Autowired
    public CarsController(CarService service,
                          AuthenticationHelper authenticationHelper,
                          CarMapper carMapper) {
        this.service = service;
        this.authenticationHelper = authenticationHelper;
        this.carMapper = carMapper;
    }

    // search by license plate, VIN or owner’s phone number
    @GetMapping
    public List<CarOutDTO> getAllCars(@RequestParam(required = false) Optional<String> search) {
        User loggedInUser = authenticationHelper.tryGetUser();
        return service.searchFromREST(search, loggedInUser).stream()
                .map(car -> carMapper.createOutDtoFromCarAndUser(car, car.getUser()))
                .collect(Collectors.toList());
    }

    @GetMapping("/filter")
    public List<CarOutDTO> filter(@RequestParam(name = "brand", required = false) Optional<String> brandName,
                            @RequestParam(name = "model", required = false) Optional<String> modelName,
                            @RequestParam(name = "year", required = false) Optional<Integer> year,
                            @RequestParam(name = "sort", required = false) Optional<String> sortOption) {
        User loggedInUser = authenticationHelper.tryGetUser();
        return service.filter(brandName, modelName, year, sortOption.map(CarSortOptions::valueOf)).stream()
                .map(car -> carMapper.createOutDtoFromCarAndUser(car, car.getUser()))
                .collect(Collectors.toList());
    }

    @PostMapping
    public CarDTO create(@Valid @RequestBody CarDTO carDTO) {
        try {
            User loggedInUser = authenticationHelper.tryGetUser();
            Car car = carMapper.createCarFromDTO(carDTO);
            service.create(loggedInUser, car);
            return carDTO;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public CarDTO update(@PathVariable int id, @Valid @RequestBody CarDTO carDTO) {
        try {
            User loggedInUser = authenticationHelper.tryGetUser();
            Car car = carMapper.createCarFromDTOUpdate(loggedInUser, carDTO, id);
            service.update(loggedInUser, car);
            return carDTO;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            User loggedInUser = authenticationHelper.tryGetUser();
            service.delete(loggedInUser, id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }


}
