package com.smartgarage.utils.mapper;

import com.smartgarage.models.attributes.Brand;
import com.smartgarage.models.attributes.CarModel;
import com.smartgarage.models.dto.BrandAndModelDTO;
import com.smartgarage.services.contracts.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class BrandAndModelMapper {

    private final BrandService brandService;

    @Autowired
    public BrandAndModelMapper(BrandService brandService) {
        this.brandService = brandService;
    }

    public Brand createBrandFromDto(BrandAndModelDTO brandAndModelDTO) {
        Brand brand = new Brand();
        brand.setName(toCapitalFirstLetter(brandAndModelDTO.getBrandName()));
        CarModel model = new CarModel();
        model.setName(toCapitalFirstLetter(brandAndModelDTO.getModelName()));
        List<CarModel> models = new ArrayList<>();
        models.add(model);
        brand.setModels(models);
        return brand;
    }

    private String toCapitalFirstLetter(String name) {
        return name.substring(0, 1).toUpperCase() +
                name.substring(1);
    }
}
