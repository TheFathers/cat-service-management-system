package com.smartgarage.utils.mapper;

import com.smartgarage.models.CarService;
import com.smartgarage.models.User;
import com.smartgarage.models.dto.CarServiceDTO;
import com.smartgarage.services.contracts.CarServiceService;
import org.springframework.stereotype.Component;

@Component
public class CarServiceMapper {

    private final CarServiceService carServiceService;

    public CarServiceMapper(CarServiceService carServiceService) {
        this.carServiceService = carServiceService;
    }


    public CarService createServiceFromDTO(CarServiceDTO serviceDTO) {
        CarService carService = new CarService();
        carService.setName(serviceDTO.getName());
        carService.setPrice(serviceDTO.getPrice());
        return carService;
    }

    public CarService createServiceFromDTOUpdate(User loggedInUser, CarServiceDTO carServiceDTO, int serviceId) {
        CarService carService = carServiceService.getById(loggedInUser, serviceId);
        carService.setName(carServiceDTO.getName());
        carService.setPrice(carServiceDTO.getPrice());
        return carService;
    }

    public CarServiceDTO createCarServiceDTOFromCarService(CarService carService) {
        CarServiceDTO carServiceDTO = new CarServiceDTO();
        carServiceDTO.setName(carService.getName());
        carServiceDTO.setPrice(carService.getPrice());
        return carServiceDTO;
    }
}
