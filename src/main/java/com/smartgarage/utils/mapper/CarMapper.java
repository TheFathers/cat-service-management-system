package com.smartgarage.utils.mapper;

import com.smartgarage.models.Car;
import com.smartgarage.models.User;
import com.smartgarage.models.attributes.Brand;
import com.smartgarage.models.attributes.CarModel;
import com.smartgarage.models.dto.CarDTO;
import com.smartgarage.models.dto.CarOutDTO;
import com.smartgarage.models.dto.UserOutDTO;
import com.smartgarage.services.contracts.BrandService;
import com.smartgarage.services.contracts.CarService;
import com.smartgarage.services.contracts.ModelService;
import com.smartgarage.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CarMapper {

    private final UserService userService;
    private final CarService carService;
    private final BrandService brandService;
    private final ModelService modelService;
    private final UserMapper userMapper;

    @Autowired
    public CarMapper(UserService userService,
                     CarService carService,
                     BrandService brandService,
                     ModelService modelService,
                     UserMapper userMapper) {
        this.userService = userService;
        this.carService = carService;
        this.brandService = brandService;
        this.modelService = modelService;
        this.userMapper = userMapper;
    }

    public Car createCarFromDTO(CarDTO carDTO) {
        Car newCar = new Car();
        newCar.setLicensePlate(carDTO.getLicensePlate());
        newCar.setVin(carDTO.getVin());
        newCar.setYear(Integer.parseInt(carDTO.getYear()));
        newCar.setBrand(new Brand(carDTO.getBrandName()));
        newCar.setCarModel(new CarModel(carDTO.getModelName()));
        newCar.setUser(userService.getByField("userName", carDTO.getUserName()));
        return newCar;
    }
    public CarDTO createCarDTOfromCar(Car car) {
        CarDTO existingCar = new CarDTO();
        existingCar.setLicensePlate(car.getLicensePlate());
        existingCar.setVin(car.getVin());
        existingCar.setYear(String.valueOf(car.getYear()));
        existingCar.setBrandName(car.getBrand().getName());
        existingCar.setModelName(car.getCarModel().getName());
        existingCar.setUserName(car.getUser().getUserName());
        return existingCar;
    }

    public Car createCarFromDTOUpdate(User loggedInUser, CarDTO carDTO, int carId) {
        Car car = carService.getById(loggedInUser, carId);
        User user = userService.getByField("userName", carDTO.getUserName());
        Brand brand = brandService.getByBrandName(carDTO.getBrandName());
        CarModel carModel = modelService.getByModelName(carDTO.getModelName());
        car.setLicensePlate(carDTO.getLicensePlate());
        car.setVin(carDTO.getVin());
        car.setYear(Integer.parseInt(carDTO.getYear()));
        car.setUser(user);
        car.setBrand(brand);
        car.setCarModel(carModel);
        return car;
    }

    public CarOutDTO createOutDtoFromCarAndUser(Car car, User user) {
        UserOutDTO userOutDTO = userMapper.userToUserOutDto(user);
        CarOutDTO carOutDTO = new CarOutDTO();
        carOutDTO.setCarId(car.getCarId());
        carOutDTO.setLicensePlate(car.getLicensePlate());
        carOutDTO.setVin(car.getVin());
        carOutDTO.setYear(car.getYear());
        carOutDTO.setBrandName(car.getBrand().getName());
        carOutDTO.setModelName(car.getCarModel().getName());
        carOutDTO.setOwner(userOutDTO);
        return carOutDTO;
    }

}