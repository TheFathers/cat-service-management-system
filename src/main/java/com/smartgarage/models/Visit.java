package com.smartgarage.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Entity
@Table(name = "visits")
public class Visit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "visit_id")
    private int visitId;

    @Column(name = "check_in")
    private LocalDateTime checkIn;

    @Column(name = "check_out")
    private LocalDateTime checkOut;

    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "car_id")
    private Car car;

    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "visits_services",
            joinColumns = @JoinColumn(name = "visit_id"),
            inverseJoinColumns = @JoinColumn(name = "service_id"))
    private List<CarService> carServices;

    public Visit() {
    }

    // constructor for instant creation of new visit
    public Visit(LocalDateTime checkIn,
                 Car car,
                 User user) {
        this.checkIn = checkIn;
        this.car = car;
        this.user = user;
    }

    public int getVisitId() {
        return visitId;
    }

    public void setVisitId(int visitId) {
        this.visitId = visitId;
    }

    public LocalDateTime getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(LocalDateTime checkIn) {
        this.checkIn = checkIn;
    }

    public LocalDateTime getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(LocalDateTime checkOut) {
        this.checkOut = checkOut;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<CarService> getServices() {
        return carServices;
    }

    public void setServices(List<CarService> carServices) {
        this.carServices = carServices;
    }

    public double getTotalPrice() {
        return carServices.stream()
                .map(CarService::getPrice)
                .reduce(0.00, Double::sum);
    }

    public String showCheckInDate() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        return checkIn.format(formatter);
    }

    public String showCheckInTime() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
        return checkIn.format(formatter);
    }

    public String showCheckOutDate() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        return checkOut.format(formatter);
    }

    public String showCheckOutTime() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
        return checkOut.format(formatter);
    }
}
