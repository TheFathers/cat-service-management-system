package com.smartgarage.models.dto;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class CarServiceDTO {

    @NotNull
    @Size(min = 2, max = 32, message = "Service name should be between 2 and 32 symbols.")
    private String name;

    @Positive
    @Digits(integer = 9, fraction = 2, message = "Invalid number (1.00 to 999999)")
    private double price;

    public CarServiceDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
