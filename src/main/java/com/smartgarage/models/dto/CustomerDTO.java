package com.smartgarage.models.dto;

import com.smartgarage.utils.validator.contracts.ValidEmail;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CustomerDTO {

    @NotNull
    @ValidEmail
    private String email;

    @NotNull
    @Size(min = 10, max = 10, message = "Phone should be at least 10 symbols")
    private String phone;

    public CustomerDTO() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
