package com.smartgarage.models.dto;

import com.smartgarage.utils.validator.contracts.FieldsValueMatch;
import com.smartgarage.utils.validator.contracts.ValidPassword;

import javax.validation.constraints.NotNull;

@FieldsValueMatch.List({
        @FieldsValueMatch(
                field = "password",
                fieldMatch = "matchingPassword",
                message = "Passwords do not match!")
})
public class PasswordDTO {

    @NotNull
    @ValidPassword
    private String password = "";

    @NotNull
    private String matchingPassword = "";

    private String passwordToken;

    public PasswordDTO() {

    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMatchingPassword() {
        return matchingPassword;
    }

    public void setMatchingPassword(String matchingPassword) {
        this.matchingPassword = matchingPassword;
    }

    public String getPasswordToken() {
        return passwordToken;
    }

    public void setPasswordToken(String passwordToken) {
        this.passwordToken = passwordToken;
    }
}
