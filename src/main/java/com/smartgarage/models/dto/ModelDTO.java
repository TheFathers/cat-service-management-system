package com.smartgarage.models.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ModelDTO {

    @NotNull
    @Size(min = 2, max = 32, message = "Model name should be between 2 and 32 symbols")
    private String modelName;

    public ModelDTO() {
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }
}
