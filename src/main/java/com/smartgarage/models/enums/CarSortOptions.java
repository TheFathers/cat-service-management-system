package com.smartgarage.models.enums;

import java.util.HashMap;
import java.util.Map;

public enum CarSortOptions {

    MODEL_ASC("Model, ascending", " order by m.name "),
    MODEL_DESC("Model, descending", " order by m.name desc "),
    BRAND_ASC("Brand, ascending", " order by b.name "),
    BRAND_DESC("Brand, descending", " order by b.name desc "),
    YEAR_ASC("Year, ascending", " order by c.year "),
    YEAR_DESC("Year, descending", " order by c.year desc ");

    private final String preview;
    private final String query;

    private static final Map<String, CarSortOptions> BY_PREVIEW = new HashMap<>();

    static {
        for (CarSortOptions option : values()) {
            BY_PREVIEW.put(option.getPreview(), option);
        }
    }

    CarSortOptions(String preview, String query) {
        this.preview = preview;
        this.query = query;
    }

    public static CarSortOptions valueOfPreview(String preview) {
        return BY_PREVIEW.get(preview);
    }

    public String getPreview() {
        return preview;
    }

    public String getQuery() {
        return query;
    }
}
