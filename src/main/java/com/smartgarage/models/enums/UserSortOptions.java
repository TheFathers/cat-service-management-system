package com.smartgarage.models.enums;

import java.util.HashMap;
import java.util.Map;

//TODO Employees must be able to sort the customers by name or visit date (latest or oldest on top).
public enum UserSortOptions {

    USERNAME_ASC("Username, ascending", " group by u.userId order by u.username "),
    USERNAME_DESC("Username, descending", " group by u.userId order by u.username desc "),
    FIRSTNAME_ASC("First name, ascending", " group by u.userId order by u.firstName "),
    FIRSTNAME_DESC("First name, descending", " group by u.userId order by u.firstName desc "),
    LASTNAME_ASC("Last name, ascending", " group by u.userId order by u.lastName "),
    LASTNAME_DESC("Last name, descending", " group by u.userId order by u.lastName desc "),
    //TODO select user from Users user join user.roles r join user.visits v where r.employeeId = 1 order by v.checkIn
    //full query to users table from UserRepositoryImpl
    VISIT_ASC("Oldest visit", " where r.employeeId = 1 order by v.checkIn "),
    VISIT_DESC("Most recent visit", " where r.employeeId = 1 order by v.checkIn desc ");

    private final String preview;
    private final String query;

    private static final Map<String, UserSortOptions> BY_PREVIEW = new HashMap<>();

    static {
        for (UserSortOptions option : values()) {
            BY_PREVIEW.put(option.getPreview(), option);
        }
    }

    UserSortOptions(String preview, String query) {
        this.preview = preview;
        this.query = query;
    }

    public static UserSortOptions valueOfPreview(String preview) {
        return BY_PREVIEW.get(preview);
    }

    public String getPreview() {
        return preview;
    }

    public String getQuery() {
        return query;
    }
}
