package com.smartgarage.repositories;

import com.smartgarage.models.Visit;
import com.smartgarage.repositories.contracts.VisitRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static java.lang.String.format;

@Repository
public class VisitRepositoryImpl extends AbstractCRUDRepository<Visit> implements VisitRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public VisitRepositoryImpl(SessionFactory sessionFactory) {
        super(Visit.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Visit> getVisitsByCarId(int carId) {
        try (Session session = sessionFactory.openSession()){
            String query = "select v from Visit v where v.car.carId = :value";
            return session.createQuery(query, Visit.class)
                    .setParameter("value", carId)
                    .list();
        }
    }

    @Override
    public List<Visit> getVisitsByCarServiceId(int serviceId) {
        try (Session session = sessionFactory.openSession()){
            String query = "select v from Visit v join v.carServices cs where cs.carServiceId = :value";
            return session.createQuery(query, Visit.class)
                    .setParameter("value", serviceId)
                    .list();
        }
    }

    @Override
    public List<Visit> search(String search) {

        final String query = "select v from Visit v " +
                "where v.user.firstName like :value " +
                "or v.user.lastName like :value " +
                "or v.user.userName like :value " +
                "or v.user.email like :value " +
                "or v.user.phone like :value " +
                "or v.car.licensePlate like :value " +
                "or v.car.vin like :value " +
                "or v.car.carModel.name like :value " +
                "or v.car.brand.name like :value";
        try (Session session = sessionFactory.openSession()){
            System.out.println(query);
            return session.createQuery(query, Visit.class)
                    .setParameter("value", "%" + search + "%")
                    .list();
        }
    }

}
