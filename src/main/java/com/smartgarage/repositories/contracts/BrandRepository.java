package com.smartgarage.repositories.contracts;

import com.smartgarage.models.attributes.Brand;

import java.util.List;

public interface BrandRepository extends BaseCRUDRepository<Brand>{

    List<Brand> getAllBrands();

    void removeModelsByBrandName(String brandName);
}
