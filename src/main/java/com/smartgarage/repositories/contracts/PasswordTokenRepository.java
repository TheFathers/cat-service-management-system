package com.smartgarage.repositories.contracts;

import com.smartgarage.models.tokens.PasswordToken;

public interface PasswordTokenRepository extends BaseCRUDRepository<PasswordToken> {
}
