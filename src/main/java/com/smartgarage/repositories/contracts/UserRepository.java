package com.smartgarage.repositories.contracts;

import com.smartgarage.models.User;

import java.util.List;

public interface UserRepository extends BaseCRUDRepository<User> {

    List<User> search(String search, List<String> parameters);

    boolean userExists(String key, String value);

    List<User> getAllCustomers();
}
