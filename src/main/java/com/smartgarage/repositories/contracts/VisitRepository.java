package com.smartgarage.repositories.contracts;

import com.smartgarage.models.Visit;

import java.util.List;

public interface VisitRepository extends BaseCRUDRepository<Visit>{
    List<Visit> getVisitsByCarId(int carId);

    List<Visit> search(String search);

    List<Visit> getVisitsByCarServiceId(int serviceId);
}
