package com.smartgarage.repositories;

import com.smartgarage.models.CarService;
import com.smartgarage.models.enums.ServiceSortOptions;
import com.smartgarage.repositories.contracts.CarServiceRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Repository
public class CarServiceRepositoryImpl extends AbstractCRUDRepository<CarService> implements CarServiceRepository {

    private final SessionFactory sessionFactory;

    public CarServiceRepositoryImpl(SessionFactory sessionFactory) {
        super(CarService.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<CarService> getServicesByCarId(int carId) {
        final String query = "select s from Visit v join v.car c join v.carServices s where v.visitId = :value";
        try (Session session = sessionFactory.openSession()){
            return session.createQuery(query, CarService.class)
                    .setParameter("value", carId)
                    .list();
        }
    }

    @Override
    public List<CarService> filter(Optional<String> name,
                                   Optional<Double> minPrice,
                                   Optional<Double> maxPrice,
                                   Optional<ServiceSortOptions> sortOptions) {
        try (Session session = sessionFactory.openSession()) {
            var baseQuery = new StringBuilder("from CarService ");
            var filters = new ArrayList<String>();
            var params = new HashMap<String, Object>();

            name.ifPresent(value -> {
                filters.add(" name like :name");
                params.put("name", "%" + value + "%");
            });

            minPrice.ifPresent(value -> {
                filters.add(" price > :minPrice");
                params.put("minPrice", value);
            });

            maxPrice.ifPresent(value -> {
                filters.add(" price < :maxPrice");
                params.put("maxPrice", value);
            });

            if (!filters.isEmpty()) {
                baseQuery.append("where ").append(String.join(" and ", filters));
            }

            sortOptions.ifPresent(value -> baseQuery.append(value.getQuery()));

            return session.createQuery(baseQuery.toString(), CarService.class)
                    .setProperties(params)
                    .list();
        }
    }

}

