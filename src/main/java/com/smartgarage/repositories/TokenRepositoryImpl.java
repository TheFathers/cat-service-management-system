package com.smartgarage.repositories;

import com.smartgarage.models.tokens.Token;
import com.smartgarage.repositories.contracts.TokenRepository;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class TokenRepositoryImpl extends AbstractCRUDRepository<Token> implements TokenRepository {
    public TokenRepositoryImpl( SessionFactory sessionFactory) {
        super(Token.class, sessionFactory);
    }
}
