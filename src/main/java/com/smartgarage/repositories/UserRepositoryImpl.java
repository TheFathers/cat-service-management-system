package com.smartgarage.repositories;

import com.smartgarage.models.User;
import com.smartgarage.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.String.format;

@Repository
public class UserRepositoryImpl extends AbstractCRUDRepository<User> implements UserRepository {

    private final SessionFactory sessionFactory;

    public UserRepositoryImpl(SessionFactory sessionFactory) {
        super(User.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> search(String search, List<String> parameters) {
        StringBuilder qeuryBuild = new StringBuilder("select u from %s u left join u.cars c where ");

        String paramsJoined = parameters.stream()
                .map(s -> String.format(" %s like :value ", s))
                .collect(Collectors.joining(" or "));

        qeuryBuild.append(paramsJoined);

        final String query = format(qeuryBuild.toString(), "User");
        try (Session session = sessionFactory.openSession()){
            return session.createQuery(query, User.class)
                    .setParameter("value", "%" + search + "%")
                    .list();
        }
    }

    @Override
    public List<User> getAllCustomers() {
        try (Session session = sessionFactory.openSession()){
            return session.createQuery(
                    "select u from User u join u.roles r where r.role = 'Customer'", User.class)
                    .list();
        }
    }

    @Override
    public boolean userExists(String key, String value) {
        final String query = format("from User u where %s like :%s ", key, "value");
        try (Session session = sessionFactory.openSession()){
            session.createQuery(query, User.class)
                    .setParameter("value", value)
                    .getSingleResult();
            return true;
        } catch (NoResultException e){
            return false;
        }
    }
}
