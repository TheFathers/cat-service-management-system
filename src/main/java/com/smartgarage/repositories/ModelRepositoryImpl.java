package com.smartgarage.repositories;

import com.smartgarage.models.attributes.CarModel;
import com.smartgarage.repositories.contracts.ModelRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ModelRepositoryImpl extends AbstractCRUDRepository<CarModel> implements ModelRepository {

    private final SessionFactory sessionFactory;

    public ModelRepositoryImpl(SessionFactory sessionFactory) {
        super(CarModel.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<CarModel> getAllModels() {
        try (Session session = sessionFactory.openSession()){
            String query = "from CarModel where isActive = true order by name";
            return session.createQuery(query, CarModel.class).list();
        }
    }

    @Override
    public List<String> getModelNamesByBrandName(String brandName) {
        try (Session session = sessionFactory.openSession()){
            String query = "select m.name from Brand b join b.models m " +
                    "where m.isActive = true and b.name like :value";
            return session.createQuery(query, String.class)
                    .setParameter("value", brandName)
                    .list();
        }
    }

    @Override
    public List<String> getModelsNotInUse() {
        try (Session session = sessionFactory.openSession()){
            String query = "select m.name from Car c right " +
                    "join c.carModel m " +
                    "where c.carId is null order by m.name";
            return session.createQuery(query, String.class)
                    .list();
        }
    }
}
