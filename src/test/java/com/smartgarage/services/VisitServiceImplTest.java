package com.smartgarage.services;

import com.posadskiy.currencyconverter.CurrencyConverter;
import com.posadskiy.currencyconverter.enums.Currency;
import com.smartgarage.exceptions.models.EntityNotFoundException;
import com.smartgarage.exceptions.models.UnauthorizedOperationException;
import com.smartgarage.models.Car;
import com.smartgarage.models.User;
import com.smartgarage.models.Visit;
import com.smartgarage.repositories.contracts.CarRepository;
import com.smartgarage.repositories.contracts.VisitRepository;
import com.smartgarage.services.contracts.PdfGenerateService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.util.*;

import static com.smartgarage.Helpers.*;

@ExtendWith(MockitoExtension.class)
class VisitServiceImplTest {

    @Mock
    VisitRepository mockVisitRepository;

    @Mock
    CurrencyConverter currencyConverter;

    @Mock
    PdfGenerateService pdfGenerateService;

    @Mock
    CarRepository carRepository;

    @InjectMocks
    VisitServiceImpl service;

    @Test
    public void getAll_should_callRepositoryWhenPerformerIsEmployee() {
        // Arrange
        User mockEmployee = createMockEmployee();

        // Act
        service.getAll(mockEmployee);

        // Assert
        Mockito.verify(mockVisitRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getAll_should_throwExceptionWhenPerformerIsNotEmployee() {
        // Arrange
        User mockUser = createMockCustomer();

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getAll(mockUser));
    }

    @Test
    public void getById_should_callRepositoryWhenPerformerIsEmployee() {
        // Arrange
        User mockEmployee = createMockEmployee();
        Visit mockVisit = createMockVisit();
        Mockito.when(mockVisitRepository.getById(Mockito.anyInt()))
                .thenReturn(mockVisit);

        // Act
        Visit result = service.getById(1, mockEmployee);

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockVisit.getVisitId(), result.getVisitId()),
                () -> Assertions.assertEquals(mockVisit.getCheckIn(), result.getCheckIn()),
                () -> Assertions.assertEquals(mockVisit.getCheckOut(), result.getCheckOut())
        );
    }

    @Test
    public void getById_should_callRepositoryWhenPerformerIsSameUser() {
        // Arrange
        User mockCustomer = createMockCustomer();
        Visit mockVisit = createMockVisit();
        mockVisit.setUser(mockCustomer);
        Mockito.when(mockVisitRepository.getById(Mockito.anyInt()))
                .thenReturn(mockVisit);

        // Act
        Visit result = service.getById(1, mockCustomer);

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockVisit.getVisitId(), result.getVisitId()),
                () -> Assertions.assertEquals(mockVisit.getCheckIn(), result.getCheckIn()),
                () -> Assertions.assertEquals(mockVisit.getCheckOut(), result.getCheckOut())
        );
    }

    @Test
    public void getById_should_throwExceptionWhenPerformerIsNotEmployeeAndNotSameUser() {
        // Arrange
        User sameUser = createMockCustomer();
        User anotherUser = createMockCustomer();
        Visit mockVisit = createMockVisit();
        mockVisit.setUser(sameUser);
        mockVisit.setVisitId(3);
        Set<Visit> visits = new HashSet<>();
        visits.add(mockVisit);
        anotherUser.setVisits(visits);

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getById(1, anotherUser));
    }

    @Test
    public void getById_should_returnVisit_when_matchExist() {
        // Arrange
        User mockCustomer = createMockCustomer();
        Visit mockVisit = createMockVisit();
        int visitId = mockVisit.getVisitId();
        Mockito.when(mockVisitRepository.getById(visitId)).thenReturn(mockVisit);
        // Act
        Visit result = service.getById(visitId, mockCustomer);

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockVisit.getVisitId(), result.getVisitId()),
                () -> Assertions.assertEquals(mockVisit.getCheckIn(), result.getCheckIn()),
                () -> Assertions.assertEquals(mockVisit.getCheckOut(), result.getCheckOut())
        );
    }

    @Test
    public void getById_should_throwException_when_visitDoesNotExist() {
        // Arrange
        User mockEmployee = createMockEmployee();

        Mockito.when(mockVisitRepository.getById(Mockito.anyInt()))
                .thenThrow(EntityNotFoundException.class);

        // Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> service.getById(Mockito.anyInt(), mockEmployee));

    }

    @Test
    public void getVisitsByCarId_should_callRepositoryWhenPerformerIsEmployee() {
        // Arrange
        Car mockCar = createMockCar();
        User mockEmployee = createMockEmployee();
        Set<Car> cars = new HashSet<>();
        cars.add(mockCar);
        mockEmployee.setCars(cars);
        int carId = mockCar.getCarId();

        // Act
        service.getVisitsByCarId(carId + 1, mockEmployee);

        // Act, Assert
        Mockito.verify(mockVisitRepository, Mockito.times(1))
                .getVisitsByCarId(carId + 1);
    }

    @Test
    public void getVisitsByCarId_should_callRepositoryWhenPerformerIsSameUser() {
        // Arrange
        Car mockCar = createMockCar();
        User mockCustomer = createMockCustomer();
        Set<Car> cars = new HashSet<>();
        cars.add(mockCar);
        mockCustomer.setCars(cars);
        int carId = mockCar.getCarId();

        // Act
        service.getVisitsByCarId(carId, mockCustomer);

        // Act, Assert
        Mockito.verify(mockVisitRepository, Mockito.times(1))
                .getVisitsByCarId(carId);
    }

    @Test
    public void getVisitsByCarId_should_throwExceptionWhenPerformerIsNotEmployeeAndNotSameUser() {
        // Arrange
        Car mockCar = createMockCar();
        User mockCustomer = createMockCustomer();
        Set<Car> cars = new HashSet<>();
        cars.add(mockCar);
        mockCustomer.setCars(cars);
        int carId = mockCar.getCarId();

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getVisitsByCarId(carId + 1, mockCustomer));
    }

    @Test
    public void getVisitsByCarId_should_returnVisit_when_matchExist() {
        // Arrange
        Car mockCar = createMockCar();
        User mockCustomer = createMockCustomer();
        Set<Car> cars = new HashSet<>();
        cars.add(mockCar);
        mockCustomer.setCars(cars);
        int carId = mockCar.getCarId();
        Visit mockVisit = createMockVisit();
        List<Visit> mockVisits = new ArrayList<>();
        mockVisits.add(mockVisit);
        Mockito.when(mockVisitRepository.getVisitsByCarId(carId)).thenReturn(mockVisits);
        // Act
        List<Visit> result = service.getVisitsByCarId(carId, mockCustomer);

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockVisits.size(), result.size())
        );
    }

    @Test
    public void create_should_callRepository_when_performerIsEmployee() {
        // Arrange
        Visit mockVisit = createMockVisit();
        User mockEmployee = createMockEmployee();

        // Act
        service.create(mockVisit, mockEmployee);

        // Assert
        Mockito.verify(mockVisitRepository, Mockito.times(1))
                .create(mockVisit);
    }

    @Test
    public void create_should_throwException_when_performerIsNotEmployee() {
        // Arrange
        Visit mockVisit = createMockVisit();
        User mockCustomer = createMockCustomer();

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.create(mockVisit, mockCustomer));
    }

    @Test
    public void update_should_callRepository_when_performerIsEmployee() {
        // Arrange
        Visit mockVisit = createMockVisit();
        User mockEmployee = createMockEmployee();

        // Act
        service.update(mockVisit, mockEmployee);

        // Assert
        Mockito.verify(mockVisitRepository, Mockito.times(1))
                .update(mockVisit);
    }

    @Test
    public void update_should_throwException_when_performerIsNotEmployee() {
        // Arrange
        Visit mockVisit = createMockVisit();
        User mockCustomer = createMockCustomer();

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.update(mockVisit, mockCustomer));
    }

    @Test
    public void delete_should_callRepository_when_performerIsEmployee() {
        // Arrange
        Visit mockVisit = createMockVisit();
        mockVisit.setCheckOut(null);
        User mockEmployee = createMockEmployee();

        // Act
        service.delete(mockVisit, mockEmployee);

        // Assert
        Mockito.verify(mockVisitRepository, Mockito.times(1))
                .delete(mockVisit);
    }

    @Test
    public void delete_should_throwException_when_performerIsNotEmployee() {
        // Arrange
        Visit mockVisit = createMockVisit();
        mockVisit.setCheckOut(null);
        User mockCustomer = createMockCustomer();

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(mockVisit, mockCustomer));
    }

    @Test
    public void delete_should_throwException_when_visitHasBeenCheckedOut() {
        // Arrange
        Visit mockVisit = createMockVisit();
        User mockEmployee = createMockEmployee();

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(mockVisit, mockEmployee));
    }

    @Test
    public void checkOut_should_callRepository_when_performerIsEmployee() {
        // Arrange
        Visit mockVisit = createMockVisit();
        int visitId = mockVisit.getVisitId();
        User mockEmployee = createMockEmployee();
        Mockito.when(mockVisitRepository.getById(visitId)).thenReturn(mockVisit);

        // Act
        service.checkOut(visitId, mockEmployee);

        // Assert
        Mockito.verify(mockVisitRepository, Mockito.times(1))
                .update(mockVisit);
    }

    @Test
    public void checkOut_should_throwException_when_performerIsNotEmployee() {
        // Arrange
        Visit mockVisit = createMockVisit();
        int visitId = mockVisit.getVisitId();
        User mockCustomer = createMockCustomer();

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.checkOut(visitId, mockCustomer));
    }

    @Test
    public void getExchangeRate_should_returnCurrencyRate_when_performerIsEmployee() {
        // Arrange
        Visit mockVisit = createMockVisit();
        User mockEmployee = createMockEmployee();
        mockVisit.setUser(mockEmployee);
        Currency mockCurrency = Currency.BGN;

        // Act
        service.getExchangeRate(mockEmployee, mockCurrency.name(), mockVisit);

        // Assert
        Mockito.verify(currencyConverter, Mockito.times(1))
                .rate(mockCurrency, mockCurrency);
    }

    @Test
    public void getExchangeRate_should_returnCurrencyRate_when_performerIsTheSameUser() {
        // Arrange
        Visit mockVisit = createMockVisit();
        User mockCustomer = createMockCustomer();
        mockVisit.setUser(mockCustomer);
        Currency mockCurrency = Currency.BGN;

        // Act
        service.getExchangeRate(mockCustomer, mockCurrency.name(), mockVisit);

        // Assert
        Mockito.verify(currencyConverter, Mockito.times(1))
                .rate(mockCurrency, mockCurrency);
    }

    @Test
    public void getExchangeRate_should_throw_when_PerformerIsNotEmployeeAndNotSameUser() {
        // Arrange
        Visit mockVisit = createMockVisit();
        User mockCustomer = createMockCustomer();
        mockCustomer.setUserId(2);
        mockVisit.setUser(createMockCustomer());
        Currency mockCurrency = Currency.BGN;

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getExchangeRate(mockCustomer, mockCurrency.name(), mockVisit));
    }

    @Test
    public void createPdf_should_returnPdfPath_when_performerIsEmployee() throws IOException {
        // Arrange
        Visit mockVisit = createMockVisit();
        User mockEmployee = createMockEmployee();
        Car mockCar = createMockCar();
        mockVisit.setUser(mockEmployee);
        mockVisit.setCar(mockCar);
        Optional<Double> mockDouble = Optional.of(2.2);
        Optional<String> mockString = Optional.of("BGN");

        // Act, Assert
        Assertions.assertDoesNotThrow(() -> service.createPdf(mockVisit, mockEmployee, mockDouble, mockString));
    }

    @Test
    public void createPdf_should_returnPdfPath_when_performerIsTheSameUser() {
        // Arrange
        Visit mockVisit = createMockVisit();
        User mockCustomer = createMockCustomer();
        Car mockCar = createMockCar();
        mockVisit.setUser(mockCustomer);
        mockVisit.setCar(mockCar);
        Optional<Double> mockDouble = Optional.of(2.2);
        Optional<String> mockString = Optional.of("BGN");

        // Act, Assert
        Assertions.assertDoesNotThrow(() -> service.createPdf(mockVisit, mockCustomer, mockDouble, mockString));
    }

    @Test
    public void createPdf_should_returnPdfPath_when_PerformerIsNotEmployeeAndNotSameUser() {
        // Arrange
        Visit mockVisit = createMockVisit();
        User mockCustomer = createMockCustomer();
        mockCustomer.setUserId(2);
        Car mockCar = createMockCar();
        mockVisit.setUser(createMockCustomer());
        mockVisit.setCar(mockCar);
        Currency mockCurrency = Currency.BGN;

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getExchangeRate(mockCustomer, mockCurrency.name(), mockVisit));
    }

}
